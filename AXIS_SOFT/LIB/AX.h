#ifndef AX_H_INCLUDED
#define AX_H_INCLUDED

#include "AXGL.h"

#include "../../LIB/LINUX/SML.h"
#include "../../LIB/LINUX/SNC.h"




class AXA{//Clase de actuador

public:
int A=0;//Valor comun
int AI=0;//Valor de la instrucción
int AS=0;//Valor de inicio de la instrucción
double AR=0;//Valor de relatividad
bool RES=false;//Posición cero.
};

class AXS{//Clase de sensor

public:
int S=0;
};

class AXNF{//Clase de datos inferidos
public:
float NF;
float NFI;
float N;
float NI;
};

class AXH{//Clase axis, conjunto de clases anteriores
public:
    
std::string AXd;
std::string AXi;

std::string CPD;//Custom program data;

SM CD;//Inicializa la memoria compartida
SM CI;

SNC IS;//Inicializa el semaforo de sincronizacion de instrucciones

/*int idse;
int idsmi;
int idsmd;

int idsnc;*/


const int STEP=800;


int FedR;
int timet;

int XFedR;
int YFedR;
int ZFedR;
int FX;
int FY;
int FZ;

const int STDFR=50;
const int SECFR=6000;
int  RAMP=1;
//const int MaxFR=1000;
//int MinFR;
//int CompFR;

AXA X;//Actuador lineal X
AXA Y;//Actuador lineal Y
AXA Z;//Actuador lineal Z

AXA S;//Actuador Velocidad motor de corte

AXA F;//Velocidad de paso

AXS A;

char tmpbuf [729];

bool RES=false;
bool EME=false;


//AXH(){}

AXH(){
    CD=SM(10255,1024);
    CI=SM(8000,1024);
    
    IS=SNC(5800,1);
  

F.AI=STDFR;
F.A=STDFR;


AXd.clear();
CD.R();
AXd+=CD.AXSM;

X.A=GETGDR("X",AXd,X.AI);
Y.A=GETGDR("Y",AXd,Y.AI);
Z.A=GETGDR("Z",AXd,Z.AI);

F.A=GETGDR("T",AXd,F.AI);

S.A=GETGDR("S",AXd,S.AI);

A.S=GETGDR("A",AXd,A.S);

AXi.clear();
CI.R();
AXi+=CI.AXSM;

X.AI=GETGDR("X",AXi,X.AI);
Y.AI=GETGDR("Y",AXi,Y.AI);
Z.AI=GETGDR("Z",AXi,Z.AI);

F.AI=GETGDR("T",AXi,F.AI);

S.AI=GETGDR("S",AXi,S.AI);


}


void uD(){//Mete las variables al bufer

// AXd.clear()
    
AXd=ADDGDS(AXd,"X",X.A);
AXd=ADDGDS(AXd,"Y",Y.A);
AXd=ADDGDS(AXd,"Z",Z.A);

AXd=ADDGDS(AXd,"S",S.A);

AXd=ADDGDS(AXd,"T",F.A);

AXd=ADDGDS(AXd,"A",A.S);
/*RPM.S=GETGDR("S",AXd,RPM.S);
WAT.S=GETGDR("P",AXd,WAT.S);
T.S=GETGDR("C",AXd,T.S);
db.S=GETGDR("D",AXd,T.S);*/

}

void rSd(){//Lee los datos y los mete al string
    AXd.clear();
    CD.R();
    AXd=CD.AXSM;
    
}

void rD(){//Lee los datos de la memoria compartida
//strncpy(tmpbuf,RSMD(idsmd,idse),sizeof(AXd));

AXd.clear();
CD.R();
    AXd=CD.AXSM;

X.A=GETGDR("X",AXd,X.A);
Y.A=GETGDR("Y",AXd,Y.A);
Z.A=GETGDR("Z",AXd,Z.A);
S.A=GETGDR("S",AXd,S.A);

F.A=GETGDR("T",AXd,F.A);

A.S=GETGDR("A",AXd,A.S);
//RPM.S=GETGDR("R",AXd,RPM.S);
//WAT.S=GETGDR("P",AXd,WAT.S);
//T.S=GETGDR("C",AXd,T.S);
//db.S=GETGDR("D",AXd,T.S);

}

void wD(){//Escribe los datos de AXH en la memoria compartida

CD.W(AXd);
}



void uI(){//Mete las variables al buffer

    if(EME){

    }
    else if(RES){

    }
    else{
        AXi.clear();
    
        AXi=ADDGDS(AXi,"X",X.AI);
        AXi=ADDGDS(AXi,"Y",Y.AI);
        AXi=ADDGDS(AXi,"Z",Z.AI);


        if(F.AI<0)F.AI=0;
            AXi=ADDGDS(AXi,"T",F.AI);


        if(S.AI<0)S.AI=0;
            AXi=ADDGDS(AXi,"S",S.AI);

        }

}
void rIa(){//Lee las instrucciones de la memoria comparida y las mete a las variables de AXH, añadiendo informacion relevante para actuación sincronizada
   int PIX=X.AI, PIY=Y.AI, PIZ=Z.AI;
    
AXi.clear();
CI.R();
    AXi=CI.AXSM;

if(GETC("stop",AXi)){
X.AI=X.A;
Y.AI=Y.A;
Z.AI=Z.A;
}

else{
X.AI=GETGDR("X",AXi,X.AI);
Y.AI=GETGDR("Y",AXi,Y.AI);
Z.AI=GETGDR("Z",AXi,Z.AI);
}

S.AI=GETGDR("S",AXi,S.AI);
if(S.AI<0)S.AI=0;


F.AS=GETGDR("T",AXi,F.AS);
if(F.AS<0)F.AS=0;

if(F.AI>F.AS){F.AI-=RAMP;}//Ramp feed rate


if(PIX!=X.AI||PIY!=Y.AI||PIZ!=Z.AI){

X.AS=X.A;
Y.AS=Y.A;
Z.AS=Z.A;

if(F.AS<SECFR){
F.AI=SECFR;

}
else{F.AI=F.AS;}
//std::cout<<"New ";

}
 
}

void rI(){//Lee las instrucciones de la memoria comparida y las mete a las variables de AXH

    
AXi.clear();
CI.R();
AXi=CI.AXSM;
   // AXi=RSMI(idsmi,idse);

if(GETC("stop",AXi)){
X.AI=X.A;
Y.AI=Y.A;
Z.AI=Z.A;
}

else{
X.AI=GETGDR("X",AXi,X.AI);
Y.AI=GETGDR("Y",AXi,Y.AI);
Z.AI=GETGDR("Z",AXi,Z.AI);
}

S.AI=GETGDR("S",AXi,S.AI);
if(S.AI<0)S.AI=0;


F.AI=GETGDR("T",AXi,F.AI);
if(F.AI<0)F.AI=0;




 
}

void rI(int w){//Lee las instrucciones de AXi y las mete a las variables de AXH

if(GETC("stop",AXi)){
X.AI=X.A;
Y.AI=Y.A;
Z.AI=Z.A;
}

else{
X.AI=GETGDR("X",AXi,X.AI);
Y.AI=GETGDR("Y",AXi,Y.AI);
Z.AI=GETGDR("Z",AXi,Z.AI);
}

S.AI=GETGDR("S",AXi,S.AI);

F.AI=GETGDR("T",AXi,F.AI);
if(F.AI<0)F.AI=0;

S.AI=GETGDR("S",AXi,S.AI);
if(S.AI<0)S.AI=0;
 


}

void wI(){//Escribe las instrucciones a la memoria compartida

CI.W(AXi);

}

bool I(){//Determina si el sistema esta esperando una instruccion o no
if(X.A==X.AI&&Y.A==Y.AI&&Z.A==Z.AI){
    if(X.A==X.AI&&Y.A==Y.AI&&Z.A==Z.AI){//Doble chequeo por si a caso
        return true;
    }
   // std::cout<<"No Inst"<<std::endl;
   // SNCB(idsnc);
    
}
else{return false;}

/*if(IPC(X.A,X.AI,4.99999)&&IPC(Y.A,Y.AI,4.99999)&&IPC(Z.A,Z.AI,4.99999)){
 //std::cout<<"No Inst"<<std::endl;
    //SNCB(idsnc);
    return true;
}
else{return false;}*/

}

bool cI(){//determina si la intrucci�on ha cambiado
    
}


void WSS(){//Escribe el archivo de estado salvado
  rI();rD();
    std::ofstream SS;
  SS.open ("SS.gs");
  SS << AXd<<std::endl<<AXi<<std::endl<<CPD;
  SS.close();
}

void RSS(){//Lee el archivo de estado guardado
    
    std::ifstream SS;
std::string line=" ";
int lineID=0;

AXd.clear();
AXi.clear();

SS.open ("SS.gs", std::ios::in|std::ios::ate);
  if (SS.is_open())  {
      
   while (!SS.eof()){
       
       getline(SS, line);
       
       if(lineID==0){
           AXd=line;
    }
    
    else if(lineID==1){
        AXi=line;
    }
    
    else if(lineID==2){
        
        CPD=line;
    }
    
    lineID++;
   }

    SS.close();
    
    lineID=0;
}


wI();

wD();

}


};

#endif
