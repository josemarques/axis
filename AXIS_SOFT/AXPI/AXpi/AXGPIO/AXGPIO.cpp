#include <iostream>
#include <sys/stat.h>
#include <sys/types.h>
#include <fcntl.h>
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <cstdlib>
#include <string>
#include <cstring>
#include <fstream>
#include <cstring>
#include <sstream>
#include <errno.h>
#include <math.h>

#include "AXSML.h"
#include "AXGL.h"
#include "AXFPL.h"
#include "AXGPL.h"

float AXREL(float s,float r,float i){
    int S=s*1000,R=r*1000,I=i*1000;
if(abs(S-I)==0) return 1;//X INSTRUCTION RELATION
else{
float TR=abs(S-R);
float TRI=abs(S-I);
float RE=TR/TRI;
/*if(RE>=1)return 1;
else*/ return RE;}
}

float AXLX=0;
float AXLY=0;
float AXLZ=0;
int   AXTD=0;
float AXS=0;

float AXLIX=0;
float AXLIY=0;
float AXLIZ=0;
int   AXTID=0;
float AXIS=0;

float AXLSX=0;
float AXLSY=0;
float AXLSZ=0;

double AXLRX=0;
double AXLRY=0;
double AXLRZ=0;

const float AXPAS=0.0050000;

int AXDEL=800;
float AXVEL;

int XEB;
int YEB;
int ZEB;

int XEC;
int YEC;
int ZEC;

int main(/*int argc, char *argv[]*/)
{
std::cout<<"AXGPIO V0.7 C++"<<std::endl;

char AXSMi [243]; for(int i=0;i<243;i++) AXSMi [i]=0;
char AXSMo [243]; for(int i=0;i<243;i++) AXSMo [i]=0;

int idse= ISE ();
int idsmi=ISMI ();
int idsmd=ISMD ();



	 //Enable GPIO pins

	if (-1 == GPIOExport(AXXP)
     || -1 == GPIOExport(AXXD)
     || -1 == GPIOExport(AXXE)

     || -1 == GPIOExport(AXYP)
     || -1 == GPIOExport(AXYD)
     || -1 == GPIOExport(AXYE)

     || -1 == GPIOExport(AXZP)
     || -1 == GPIOExport(AXZD)
     || -1 == GPIOExport(AXZE)
     )
		return(1);


	 // Set GPIO directions

	if (-1 == GPIODirection(AXXP, OUT)
     || -1 == GPIODirection(AXXD, OUT)
     || -1 == GPIODirection(AXXE, IN)

     || -1 == GPIODirection(AXYP, OUT)
     || -1 == GPIODirection(AXYD, OUT)
     || -1 == GPIODirection(AXYE, IN)

     || -1 == GPIODirection(AXZP, OUT)
     || -1 == GPIODirection(AXZD, OUT)
     || -1 == GPIODirection(AXZE, IN) )

     {      if (-1 == GPIODirection(AXXP, OUT)
     || -1 == GPIODirection(AXXD, OUT)
     || -1 == GPIODirection(AXXE, IN)

     || -1 == GPIODirection(AXYP, OUT)
     || -1 == GPIODirection(AXYD, OUT)
     || -1 == GPIODirection(AXYE, IN)

     || -1 == GPIODirection(AXZP, OUT)
     || -1 == GPIODirection(AXZD, OUT)
     || -1 == GPIODirection(AXZE, IN)
     )
		return(2);}


strncpy(AXSMo, RSMD(idsmd, idse), sizeof(AXSMo));

AXLX=GETGDR("X",AXSMo,AXLX);
AXLY=GETGDR("Y",AXSMo,AXLY);
AXLZ=GETGDR("Z",AXSMo,AXLZ);
AXDEL=GETGDR("T",AXSMo,AXDEL);

strncpy(AXSMi, RSMI(idsmi, idse), sizeof(AXSMi));

AXLIX=GETGDR("X",AXSMi,AXLIX);
AXLIY=GETGDR("Y",AXSMi,AXLIY);
AXLIZ=GETGDR("Z",AXSMi,AXLIZ);
AXDEL=GETGDR("T",AXSMi,AXDEL);


		//XEB = GPIORead(AXXE);
		//YEB = GPIORead(AXYE);
		//ZEB = GPIORead(AXZE);


	while(1) {

if(FPC(AXLX,AXLIX,0.005)&&FPC(AXLY,AXLIY,0.005)&&FPC(AXLZ,AXLIZ,0.005)) {    //NO INSTRUCCION EXECUTION
strncpy(AXSMi, RSMI(idsmi, idse), sizeof(AXSMi));

AXLIX=GETGDR("X",AXSMi,AXLIX);
AXLIY=GETGDR("Y",AXSMi,AXLIY);
AXLIZ=GETGDR("Z",AXSMi,AXLIZ);
AXDEL=GETGDR("T",AXSMi,AXDEL);

//AXLIX=RFP(AXLIX,AXPAS);
//AXLIY=RFP(AXLIY,AXPAS);
//AXLIZ=RFP(AXLIZ,AXPAS);
 AXLIX=roundf(AXLIX*1000)/1000.0;
 AXLIY=roundf(AXLIY*1000)/1000.0;
 AXLIZ=roundf(AXLIZ*1000)/1000.0;

AXLSX=AXLX;
AXLSY=AXLY;
AXLSZ=AXLZ;

for(int i=0;i<243;i++) AXSMi [i]=0;

usleep(100000);
//END RESET SHARED INPUT
}
                                                               //END NO INSTRUCTION EXECUTOR
else{

strncpy(AXSMi, RSMI(idsmi, idse), sizeof(AXSMi));
 if(GETC("stop",AXSMi) ){AXLIX=AXLX;AXLIY=AXLY;AXLIZ=AXLZ;}
 else if(GETC("reset",AXSMi)){
 AXLX=0; AXLY=0; AXLZ=0;
 AXLIX=0; AXLIY=0; AXLIZ=0;}


  //  AXLX=RFP(AXLX,AXPAS);
//AXLY=RFP(AXLY,AXPAS);
//AXLZ=RFP(AXLZ,AXPAS);                                                      //INSTRUCTION EXECUTOR

AXLRX=AXREL(AXLSX,AXLX,AXLIX);
AXLRY=AXREL(AXLSY,AXLY,AXLIY);
AXLRZ=AXREL(AXLSZ,AXLZ,AXLIZ);
//std::cout<<AXLRX<<" "<<AXLRY<<" "<<AXLRZ<<"\n";//RELATIVITY SECURE COMPROBATOR

//RELATIVE EXECUTOR
if(AXLX<AXLIX&&AXLRX<=AXLRY&&AXLRX<=AXLRZ){
if (-1 == GPIOWrite(AXXD, LOW))
		return(3);
		if (-1 == GPIOWrite(AXXP, HIGH))
		return(3);
usleep(AXDEL);
		if (-1 == GPIOWrite(AXXP, LOW))
		return(3);
AXLX+=AXPAS;}

else if(AXLX>AXLIX&&AXLRX<=AXLRY&&AXLRX<=AXLRZ){
    if (-1 == GPIOWrite(AXXD, HIGH))
		return(3);
 		if (-1 == GPIOWrite(AXXP, HIGH))
		return(3);
usleep(AXDEL);
		if (-1 == GPIOWrite(AXXP, LOW))
		return(3);
		if (-1 == GPIOWrite(AXXD, LOW))
		return(3);
AXLX-=AXPAS;}


else if(AXLY<AXLIY&&AXLRY<=AXLRX&&AXLRY<=AXLRZ){
if (-1 == GPIOWrite(AXYD, LOW))
		return(3);
		if (-1 == GPIOWrite(AXYP, HIGH))
		return(3);
usleep(AXDEL);
		if (-1 == GPIOWrite(AXYP, LOW))
		return(3);
AXLY+=AXPAS;}

else if(AXLY>AXLIY&&AXLRY<=AXLRX&&AXLRX<=AXLRZ){
    if (-1 == GPIOWrite(AXYD, HIGH))
		return(3);
		if (-1 == GPIOWrite(AXYP, HIGH))
		return(3);
usleep(AXDEL);
		if (-1 == GPIOWrite(AXYP, LOW))
		return(3);
		if (-1 == GPIOWrite(AXYD, LOW))
		return(3);
AXLY-=AXPAS;}


else if(AXLZ<AXLIZ&&AXLRZ<=AXLRY&&AXLRZ<=AXLRX){
		if (-1 == GPIOWrite(AXZP, LOW))
		return(3);
usleep(AXDEL);
		if (-1 == GPIOWrite(AXZP, HIGH))
		return(3);
AXLZ+=AXPAS;}

else if(AXLZ>AXLIZ&&AXLRZ<=AXLRY&&AXLRZ<=AXLRX){
    if (-1 == GPIOWrite(AXZD, HIGH))
		return(3);
		if (-1 == GPIOWrite(AXZP, LOW))
		return(3);
usleep(AXDEL);
		if (-1 == GPIOWrite(AXZP, HIGH))
		return(3);
		if (-1 == GPIOWrite(AXZD, LOW))
		return(3);
AXLZ-=AXPAS;}

else{ if(AXLX<AXLIX){
if (-1 == GPIOWrite(AXXD, LOW))
		return(3);
		if (-1 == GPIOWrite(AXXP, HIGH))
		return(3);
usleep(AXDEL);
		if (-1 == GPIOWrite(AXXP, LOW))
		return(3);
AXLX+=AXPAS;
}

else if(AXLX>AXLIX){
 if (-1 == GPIOWrite(AXXD, HIGH))
		return(3);
 		if (-1 == GPIOWrite(AXXP, HIGH))
		return(3);
usleep(AXDEL);
		if (-1 == GPIOWrite(AXXP, LOW))
		return(3);
		if (-1 == GPIOWrite(AXXD, LOW))
		return(3);
AXLX-=AXPAS;
}

 if(AXLY<AXLIY){
if (-1 == GPIOWrite(AXYD, LOW))
		return(3);
		if (-1 == GPIOWrite(AXYP, HIGH))
		return(3);
usleep(AXDEL);
		if (-1 == GPIOWrite(AXYP, LOW))
		return(3);
AXLY+=AXPAS;
}

else if(AXLY>AXLIY){
 if (-1 == GPIOWrite(AXYD, HIGH))
		return(3);
 		if (-1 == GPIOWrite(AXYP, HIGH))
		return(3);
usleep(AXDEL);
		if (-1 == GPIOWrite(AXYP, LOW))
		return(3);
		if (-1 == GPIOWrite(AXYD, LOW))
		return(3);
AXLY-=AXPAS;
}

 if(AXLZ<AXLIZ){
if (-1 == GPIOWrite(AXZD, LOW))
		return(3);
		if (-1 == GPIOWrite(AXZP, HIGH))
		return(3);
usleep(AXDEL);
		if (-1 == GPIOWrite(AXZP, LOW))
		return(3);
AXLZ+=AXPAS;
}

else if(AXLZ>AXLIZ){
 if (-1 == GPIOWrite(AXZD, HIGH))
		return(3);
 		if (-1 == GPIOWrite(AXZP, HIGH))
		return(3);
usleep(AXDEL);
		if (-1 == GPIOWrite(AXZP, LOW))
		return(3);
		if (-1 == GPIOWrite(AXZD, LOW))
		return(3);
AXLZ-=AXPAS;
}}
//END RELATIVE EXECUTOR

AXLX=roundf(AXLX*1000)/1000.0;
 AXLY=roundf(AXLY*1000)/1000.0;
 AXLZ=roundf(AXLZ*1000)/1000.0;

strncpy(AXSMo, RSMD(idsmd, idse), 243);
strncpy(AXSMo,ADDGDS(AXSMo,"X",AXLX),243);
strncpy(AXSMo,ADDGDS(AXSMo,"Y",AXLY),243);
strncpy(AXSMo,ADDGDS(AXSMo,"Z",AXLZ),243);
strncpy(AXSMo,ADDGDS(AXSMo,"T",AXDEL),243);

WSMD(AXSMo,idsmd, idse);

//std::cout<<"AXI: X "<<AXLIX<<" Y "<<AXLIY<<" Z "<<AXLIZ<<"\n";
//std::cout<<"AXD: "<<"X "<<AXLX<<" Y "<<AXLY<<" Z "<<AXLZ<<"\n";
//AXIS SHARED OUTPUT WRITE

}

                                                         //END INSTRUCCION EXECUTOR
	}



	 // Disable GPIO pins

	if (-1 == GPIOUnexport(AXXP)
     || -1 == GPIOUnexport(AXXD)
     || -1 == GPIOUnexport(AXXE)

     || -1 == GPIOUnexport(AXYP)
     || -1 == GPIOUnexport(AXYD)
     || -1 == GPIOUnexport(AXYE)

     || -1 == GPIOUnexport(AXZP)
     || -1 == GPIOUnexport(AXZD)
     || -1 == GPIOUnexport(AXZE)
     )
		return(4);

	return(0);
}
