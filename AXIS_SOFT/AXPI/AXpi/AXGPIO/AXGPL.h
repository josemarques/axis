#ifndef AXGPL_H_INCLUDED
#define AXGPL_H_INCLUDED

#include <iostream>
#include <sys/stat.h>
#include <sys/types.h>
#include <fcntl.h>
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <cstdlib>
#include <string>
#include <cstring>
#include <fstream>
#include <cstring>
#include <sstream>
#include <errno.h>
#include <math.h>

#define IN  0
#define OUT 1

#define LOW  0
#define HIGH 1


#define AXXP 21 //GPIO C 36
#define AXXD 20 /* P1-07 */
#define AXXE 16  //GPIO C 37

#define AXYP 19 //GPIO C 38
#define AXYD 26  /* P1-07 */
#define AXYE 13  //GPIO C 35

#define AXZP 6 //GPIO C 40
#define AXZD 5  /* P1-07 */
#define AXZE 12  //GPIO C 33

static int GPIOExport(int pin)                    //GPIO SETUP
{
#define BUFFER_MAX 3
	char buffer[BUFFER_MAX];
	ssize_t bytes_written;
	int fd;

	fd = open("/sys/class/gpio/export", O_WRONLY);
	if (-1 == fd) {
	    std::cout<< stderr << "GPIO EXPORT ERROR\n";
		return(-1);
	}

	bytes_written = snprintf(buffer, BUFFER_MAX, "%d", pin);
	write(fd, buffer, bytes_written);
	close(fd);
	return(0);
}

static int GPIOUnexport(int pin)
{
	char buffer[BUFFER_MAX];
	ssize_t bytes_written;
	int fd;

	fd = open("/sys/class/gpio/unexport", O_WRONLY);
	if (-1 == fd) {
		std::cout<< stderr << "GPIO UNEXPORT ERROR\n";
		return(-1);
	}

	bytes_written = snprintf(buffer, BUFFER_MAX, "%d", pin);
	write(fd, buffer, bytes_written);
	close(fd);
	return(0);
}

static int GPIODirection(int pin, int dir)
{
	static const char s_directions_str[]  = "in\0out";

#define DIRECTION_MAX 35
	char path[DIRECTION_MAX];
	int fd;

	snprintf(path, DIRECTION_MAX, "/sys/class/gpio/gpio%d/direction", pin);
	fd = open(path, O_WRONLY);
	if (-1 == fd) {
		std::cout<< stderr << "GPIO DIRECTION ERROR\n";
		return(-1);
	}

	if (-1 == write(fd, &s_directions_str[IN == dir ? 0 : 3], IN == dir ? 2 : 3)) {
		std::cout<< stderr << "GPIO DIRECTION 2 ERROR\n";
		return(-1);
	}

	close(fd);
	return(0);
}

static int GPIORead(int pin)
{
#define VALUE_MAX 30
	char path[VALUE_MAX];
	char value_str[3];
	int fd;

	snprintf(path, VALUE_MAX, "/sys/class/gpio/gpio%d/value", pin);
	fd = open(path, O_RDONLY);
	if (-1 == fd) {
		std::cout<< stderr << "GPIO READ ERROR\n";
		return(-1);
	}

	if (-1 == read(fd, value_str, 3)) {
		std::cout<< stderr << "GPIO READ 2 ERROR\n";
		return(-1);
	}

	close(fd);

	return(atoi(value_str));
}

static int GPIOWrite(int pin, int value)
{
	static const char s_values_str[] = "01";

	char path[VALUE_MAX];
	int fd;

	snprintf(path, VALUE_MAX, "/sys/class/gpio/gpio%d/value", pin);
	fd = open(path, O_WRONLY);
	if (-1 == fd) {
		std::cout<< stderr << "GPIO WRITE ERROR\n";
		return(-1);
	}

	if (1 != write(fd, &s_values_str[LOW == value ? 0 : 1], 1)) {
		std::cout<< stderr << "GPIO WRITE 2 ERROR\n";
		return(-1);
	}

	close(fd);
	return(0);
}                                                                        //END GPIO SETUP




#endif // AXGPL_H_INCLUDED
