#include <iostream>
#include <fstream>
#include <string>
#include <cstring>
#include <sstream>
#include <stdlib.h>
#include <iostream>

#include <math.h>

#include <sys/types.h>
#include <unistd.h>
#include <string>
#include <cstring>
#include <stdio.h>
#include <stdlib.h>
#include <sstream>
#include <errno.h>

#include "AXSML.h"
#include "AXFPL.h"
#include "AXGL.h"



float AXLIX=0,AXLIY=0,AXLIZ=0,AXLIC=0,AXTIP=0,AXAIU=0,AXAIV=0,AXAIW=0;
float AXLX=0,AXLY=0,AXLZ=0,AXLC=0,AXTP=0,AXAU=0,AXAV=0,AXAW=0;

int main() {

char AXSMi [243]; for(int i=0;i<243;i++) AXSMi [i]=0;
char AXSMo [243]; for(int i=0;i<243;i++) AXSMo [i]=0;

int idse= ISE ();
int idsmi=ISMI ();
int idsmd=ISMD ();

//START AXPROGS
//system("AXIS/AXIS/AXGPIO");
//END START AXPROGS


//START AXIS PROGRAM


std::cout<<"AXG V0.5 C++"<<"\n";

strncpy(AXSMi, RSMD(idsmd, idse), sizeof(AXSMi));
    AXLIX=GETGDR("X",AXSMi,AXLIX);
    AXLIY=GETGDR("Y",AXSMi,AXLIY);
    AXLIZ=GETGDR("Z",AXSMi,AXLIZ);


std::ifstream AXG("AXG.ngc");
std::string AXGL ;


while( std::getline( AXG, AXGL ) ) {

   // getchar();

        std::cout<<AXGL<<'\n';

std::stringstream inst;
  inst<<AXGL;

    while (inst!=0){

      std::string a;
    inst>>a;
    std::cout<<a<<'\n';



 if (a.compare("G0") == 0){
    std::cout << "tool off fast move\n";
    }
 else if (a.compare("G1") == 0){
    std::cout << "tool on feed move\n";
    }

  else if (a.compare("G20") == 0){
    std::cout << "inches\n";
    }

    else if (a.compare("G21") == 0){
    std::cout << "mm\n";
    }

       else if (a.compare("G90") == 0){
    std::cout << "absolute coords\n";
    }

       else if (a.compare("G91") == 0){
    std::cout << "incremental coords\n";
    }

else if (a.compare("M2") == 0){
    std::cout << "end program\n";
    }

  else if (a.compare("M3") == 0){
    std::cout << "tool on cw\n";
    }

    else if (a.compare("M4") == 0){
    std::cout << "tool on ccw\n";
    }

    else if (a.compare("M5") == 0){
    std::cout << "tool off\n";
    }

   else if (a.compare("C0") == 0){
    std::cout << "close cacher\n";
    }

  else if (a.compare("C1") == 0){
    std::cout << "open cacher\n";
    }

 else if (a.compare(0,1,"X") == 0){
     a.erase(0,1);
     char in[10];
     strcpy(in, a.c_str());
     AXLIX = strtod (in,0);

  //  std::cout << "X INST \n";
    }

   else if (a.compare(0,1,"Y") == 0){
     a.erase(0,1);
     char in[10];
     strcpy(in, a.c_str());
     AXLIY = strtod (in,0);
    // AXLIY = AXLIY*100;
    // AXLIY = round(AXLIY)*10;
    // AXLIY /=1000;
  //  std::cout << "Y INST \n";
    }

  else if (a.compare(0,1,"Z") == 0){
     a.erase(0,1);
     char in[10];
     strcpy(in, a.c_str());
     AXLIZ = strtod (in,0);

  //  std::cout << "Z INST \n";
    }

else if (a.compare(0,1,"U") == 0){
     a.erase(0,1);
     char in[10];
     strcpy(in, a.c_str());
     AXAIU = strtod (in,0);
  //  std::cout << "U INST \n";
    }

else if (a.compare(0,1,"V") == 0){
     a.erase(0,1);
     char in[10];
     strcpy(in, a.c_str());
     AXAIV = strtod (in,0);
 //   std::cout << "V INST \n";
    }

else if (a.compare(0,1,"W") == 0){
     a.erase(0,1);
     char in[10];
     strcpy(in, a.c_str());
     AXAIW = strtod (in,0);
 //   std::cout << "W INST \n";
    }

else if (a.compare(0,1,"F") == 0){
     a.erase(0,1);
     char in[20];
     strcpy(in, a.c_str());
     AXLIC = strtod (in,0);
 //   std::cout << "C INST \n";
    }

    else if (a.compare(0,2,"ST") == 0){

    }

else if (a.compare(0,1,"S") == 0){
     a.erase(0,1);
     char in[20];
     strcpy(in, a.c_str());
     AXTIP = strtod (in,0);
 //   std::cout << "S INST \n";
    }



}




std::cout<<"X"<<AXLIX<<" Y"<<AXLIY<<" Z"<<AXLIZ<<"\n";
std::cout<<"U"<<AXAIU<<" V"<<AXAIV<<" W"<<AXAIW<<"\n";
std::cout<<"C"<<AXLIC<<" S"<<AXTIP<<"\n"<<"\n";

strncpy(AXSMo, RSMD(idsmd, idse), 243);
strncpy(AXSMo,ADDGDS(AXSMo,"X",AXLIX),243);
strncpy(AXSMo,ADDGDS(AXSMo,"Y",AXLIY),243);
strncpy(AXSMo,ADDGDS(AXSMo,"Z",AXLIZ),243);

WSMI(AXSMo,idsmi, idse);

//for(int i=0;i<243;i++) AXSMo [i]=0;



//END AXIS SHARED OUTPUT WRITE


usleep(50000);

while(!FPC(AXLX,AXLIX,0.1)||!FPC(AXLY,AXLIY,0.1)||!FPC(AXLZ,AXLIZ,0.1)){//INSTRUCTION COMPARATOR
    //std::cout<<AXSMi<<"\n";

    strncpy(AXSMi, RSMD(idsmd, idse), sizeof(AXSMi));
    AXLX=GETGDR("X",AXSMi,AXLX);
    AXLY=GETGDR("Y",AXSMi,AXLY);
    AXLZ=GETGDR("Z",AXSMi,AXLZ);



std::cout<<"AXOUT: X "<<AXLIX<<" Y "<<AXLIY<<" Z "<<AXLIZ<<"\n";
std::cout<<"AXIN: "<<"X "<<AXLX<<" Y "<<AXLY<<" Z "<<AXLZ<<"\n";
for(int i=0;i<243;i++) AXSMi [i]=0;


                //END INSTRUCTION PARSER
                                                                        //END REAL READ
usleep(50000);}//END INSTRUCTION COMPARATOR


}
    return 0;
}
