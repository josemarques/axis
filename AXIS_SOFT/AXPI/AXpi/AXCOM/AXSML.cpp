#include <sys/types.h>
#include <sys/ipc.h>
#include <sys/shm.h>
#include <sys/sem.h>
#include <unistd.h>
#include <string>
#include <cstring>
#include <stdio.h>
#include <stdlib.h>
#include <sstream>
#include <errno.h>
#include <iostream>

#define AXSMZd    243
#define AXSMZi    243

#define MAX_RETRIES 10

union semun {
    int val;               /* used for SETVAL only */
    struct semid_ds *buf;  /* used for IPC_STAT and IPC_SET */
    ushort *array;         /* used for GETALL and SETALL */
};

int AXSMIDd;
char *AXSHMd;
char *AXSMSd;

int AXSMIDi;
char *AXSHMi;
char *AXSMSi;

struct semid_ds buf;
union semun arg;
struct sembuf sb;


int ISMD (){
// INITIALIZE SHARED MEMORY INPUT

key_t smkey=8244;

AXSMIDd=shmget(smkey,AXSMZd,IPC_CREAT | 0666);
    if(AXSMIDd<0){
        std::cerr<<"semget1";
        exit(1);
        }



AXSHMd=(char*)shmat(AXSMIDd,0,0);
   if(AXSHMd==(char*)-1){
std::cerr<<"shmat1";
        exit(1);
}
return AXSMIDd;
// END INITIALIZE SHARED MEMORY INPUT

}

int ISMI (){
// INITIALIZE SHARED MEMORY INPUT

key_t smkey=8000;

AXSMIDi=shmget(smkey,AXSMZi,IPC_CREAT | 0666);
    if(AXSMIDi<0){
        std::cerr<<"semget1";
        exit(1);
        }



AXSHMi=(char*)shmat(AXSMIDi,0,0);
   if(AXSHMi==(char*)-1){
std::cerr<<"shmat1";
        exit(1);
}
return AXSMIDi;
// END INITIALIZE SHARED MEMORY INPUT

}

int ISE (){
key_t semkey=3800;
int semid;
struct semid_ds buf;

union semun arg;
struct sembuf sb;

sb.sem_op = -1;
sb.sem_flg = /*SEM_UNDO;*/IPC_NOWAIT;

//sb.sem_flg = 0;
arg.val = 1;

semid = semget(semkey, 2, IPC_CREAT | IPC_EXCL | 0666);
 if (semid >= 0) { /* we got it first */
        sb.sem_op = 1; //sb.sem_flg = 0;
        arg.val = 1;

        for(sb.sem_num = 0; sb.sem_num < 2; sb.sem_num++) {
            //do a semop() to "free" the semaphores.
            // this sets the sem_otime field, as needed below.
            if (semop(semid, &sb, 1) == -1) {
                int e = errno;
                semctl(semid, 0, IPC_RMID); // clean up
                errno = e;
                return -1; // error, check errno
            }
        }

} else if (errno == EEXIST) { /* someone else got it first */
        int ready = 0;

        semid = semget(semkey, 2, 0); /* get the id */
        if (semid < 0) return semid; /* error, check errno */

        /* wait for other process to initialize the semaphore: */
        arg.buf = &buf;
        for(int i = 0; i < MAX_RETRIES && !ready; i++) {
            semctl(semid, 2, IPC_STAT, arg);
            if (arg.buf->sem_otime != 0) {
                ready = 1;
            } else {
                sleep(1);
            }
        }
        if (!ready) {
            errno = ETIME;
            return -1;
        }
    }

return semid;
//END INITIALIZE SEMAPHORES
}

char *RSMI(int siid, int seid){
int sen=0;
int AXSMi=0;
char* AXSM = new char[243];for(int w=0;w<243;w++)AXSM[w]=0;

sb.sem_op = -1;//BLOCK SHM IN
sb.sem_num = sen;
    if (semop(seid, &sb, 1) == -1) {
             std::cout<<"SEM NO AVALIABLE\n";
        //return 1;
        }//END BLOCK SHM IN

else{

//READ SHARED INPUT
for(AXSMSi=AXSHMi;*AXSMSi!=0;AXSMSi++){
AXSM[AXSMi]=*AXSMSi;
AXSMi++;}
AXSMi=0;

  sb.sem_op = 1;//FREE SHM IN
sb.sem_num =sen;
    if (semop(seid, &sb, 1) == -1) {
        perror("semop");
        exit(1);
    }//END FREE SHM IN

}AXSM[242]='\n';
return AXSM ;
}

char *RSMD(int sdid, int seid){
int sen=1;
int AXSMi=0;
char* AXSM = new char[243];for(int w=0;w<243;w++)AXSM[w]=0;

sb.sem_op = -1;//BLOCK SHM IN
sb.sem_num = sen;
    if (semop(seid, &sb, 1) == -1) {
             std::cout<<"SEM NO AVALIABLE\n";
        //return 1;
        }//END BLOCK SHM IN

else{

//READ SHARED INPUT
for(AXSMSd=AXSHMd;*AXSMSd!=0;AXSMSd++){
AXSM[AXSMi]=*AXSMSd;
AXSMi++;}
AXSMi=0;

  sb.sem_op = 1;//FREE SHM IN
sb.sem_num =sen;
    if (semop(seid, &sb, 1) == -1) {
        perror("semop");
        exit(1);
    }//END FREE SHM IN

}
return AXSM ;
}

void WSMI(char data[243],int siid, int seid){
//AXIS SHARED WRITE
int sen=0;
union semun arg;
struct sembuf sb;

sb.sem_op = -1;
sb.sem_num = sen;
    if (semop(seid, &sb, 1) == -1) {
        std::cout<<"SEM NO AVALIABLE\n";

        }
        else{

std::memcpy(AXSHMi,data,243);
 AXSMSi=AXSHMi;
 AXSMSi+=243;
  *AXSMSi=0;

sb.sem_op = 1;//FREE SHM OUT
sb.sem_num = sen;

    if (semop(seid, &sb, 1) == -1) {
         std::cout<<"SEM NO AVALIABLE\n";
        //return 1;
        }//END FREE SHM OUT

        }

//END AXIS SHARED  WRITE
}

void WSMD(char data[243],int sdid, int seid){
//AXIS SHARED WRITE
int sen=1;
union semun arg;
struct sembuf sb;

sb.sem_op = -1;
sb.sem_num = sen;
    if (semop(seid, &sb, 1) == -1) {
        std::cout<<"SEM NO AVALIABLE\n";

        }
        else{

std::memcpy(AXSHMd,data,243);
 AXSMSd=AXSHMd;
 AXSMSd+=243;
  *AXSMSd=0;

sb.sem_op = 1;//FREE SHM OUT
sb.sem_num = sen;

    if (semop(seid, &sb, 1) == -1) {
         std::cout<<"SEM NO AVALIABLE\n";
        //return 1;
        }//END FREE SHM OUT

        }

//END AXIS SHARED  WRITE
}

int DSM (key_t smkey){

}

int DSE (key_t semkey){

}
