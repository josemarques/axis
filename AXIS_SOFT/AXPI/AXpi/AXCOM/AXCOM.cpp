#include <stdio.h>      // standard input / output functions
#include <stdlib.h>
#include <string.h>     // string function definitions
#include <unistd.h>     // UNIX standard function definitions
#include <fcntl.h>      // File control definitions
#include <errno.h>      // Error number definitions
#include <termios.h>
#include <iostream>

#include "AXGL.h"
#include "AXSML.h"

float AXTS;
float AXTIS;

char data [243];
char inst [243];

std::string datas;
std::string insts;



/* Open File Descriptor */
int main(){

int idse= ISE ();
int idsmi=ISMI ();
int idsmd=ISMD ();

int USB = open( "/dev/ttyACM0", O_RDWR| O_NOCTTY );

/* Error Handling */
if ( USB < 0 )
{
std::cout << "Error " << errno << " opening " << "/dev/ttyACM0" << ": " << strerror (errno) << std::endl;
}

/* *** Configure Port *** */
struct termios tty;
memset (&tty, 0, sizeof tty);

/* Error Handling */
if ( tcgetattr ( USB, &tty ) != 0 )
{
std::cout << "Error " << errno << " from tcgetattr: " << strerror(errno) << std::endl;
}

/* Set Baud Rate */
cfsetispeed (&tty, B9600);
cfsetispeed (&tty, B9600);
/* Setting other Port Stuff */
tty.c_cflag     &=  ~PARENB;        // Make 8n1
tty.c_cflag     &=  ~CSTOPB;
tty.c_cflag     &=  ~CSIZE;
tty.c_cflag     |=  CS8;
tty.c_cflag     &=  ~CRTSCTS;       // no flow control
tty.c_lflag     =   0;          // no signaling chars, no echo, no canonical processing
tty.c_oflag     =   ~OPOST;                  // no remapping, no delays
tty.c_cc[VMIN]      =  11;                  // read doesn't block
tty.c_cc[VTIME]     =   7;                  // 0.5 seconds read timeout

tty.c_cflag     |=  CREAD | CLOCAL;     // turn on READ & ignore ctrl lines
tty.c_iflag     &=  ~(IXON | IXOFF | IXANY);// turn off s/w flow ctrl
tty.c_lflag     &=  ~(ICANON | ECHO | ECHOE | ISIG); // make raw
tty.c_oflag     &=  ~OPOST;              // make raw

/* Flush Port, then applies attributes */
tcflush( USB, TCIFLUSH );
if ( tcsetattr ( USB, TCSANOW, &tty ) != 0)
{
std::cout << "Error " << errno << " from tcsetattr" << std::endl;
}
//char cmd[243];for (int i=0;i<21;i++)cmd[i]=0;


while(1){

strncpy(inst, RSMI(idsmi, idse), sizeof(inst));
 AXTIS=GETGDS("S",inst);
std::cout << "GLIVEI " << inst<< " SI " << AXTIS <<"\n";



char cmd[] = "S1222.45 \n";
strcpy(cmd, ADDGDS (cmd,"S",AXTIS));
/* *** WRITE *** */
int n_written = write( USB, cmd, sizeof(cmd)-1 );
//std::cout << "cmd " << cmd <<"\n";
/* *** READ *** */
char buf [21];
memset (&buf, '\0', sizeof buf);
int n = read( USB, &buf , sizeof (buf) );
if (n < 0) {std::cout << "Error reading: " << strerror(errno) << std::endl;}
/* *** READ *** */


/* Print what I read... */
//std::cout << "IN " << buf <<" n "<<n<< "\n";

AXTS=GETGDS("S",buf);

strcpy(data, RSMD(idsmd, idse));

//const char *ADDGDS (char current[243],std::string id,float data);
strcpy(data, ADDGDS (data,"S",AXTS));
 //strncpy ( data, ADDGD (data,'S',AXTS), sizeof(data) );
//strcpy(data, ADDGD (data,'S',AXTS));
WSMD(data,idsmd, idse);


std::cout << "GLIVED " << data<<" S " <<AXTS<< std::endl;

}
close(USB);
}
