#include <stdio.h>      // standard input / output functions
#include <stdlib.h>
#include <string.h>     // string function definitions
#include <unistd.h>     // UNIX standard function definitions
#include <fcntl.h>      // File control definitions
#include <errno.h>      // Error number definitions
#include <termios.h>
#include <iostream>
#include <math.h>

//#include <sys/time.h> 
//struct timeval t1, t2;
//double elapsedTime;

#include "../LIB/AX.h"


int n_written=0;
int n=0;

bool run=true;


int main(){
std::cout << "AXSCOM V0.42 C++" << std::endl;

char bufc[1];
uint8_t RD[3];

const int NBC=4;
int byteC=0;

long NMEAS=0;

std::string SerialData;



int idse= ISE ();
int idsmi=ISMI ();
int idsmd=ISMD ();

int idsnc=SNCI();//Semaforo en 0 supuestamente


AXH AXIS(idse,idsmi,idsmd/*, idsnc*/);

int USB = open( "/dev/ttyACM1", O_RDONLY| O_NOCTTY );

/* Error Handling */
if ( USB < 0 ){
std::cout << "Error " << errno << " opening " << "/dev/ttyACM0" << ": " << strerror (errno) << std::endl;
run=false;
}

/* *** Configure Port *** */
struct termios tty;
memset (&tty, 0, sizeof tty);

/* Error Handling */
if ( tcgetattr ( USB, &tty ) != 0 )
std::cout << "Error " << errno << " from tcgetattr: " << strerror(errno) << std::endl;

/* Set Baud Rate */
cfsetispeed (&tty, B921600);
cfsetispeed (&tty, B921600);
/* PORT CONFIG */
tty.c_cflag     &=  ~PARENB;        // Make 8n1
tty.c_cflag     &=  ~CSTOPB;
tty.c_cflag     &=  ~CSIZE;
tty.c_cflag     |=  CS8;
tty.c_cflag     |=  CRTSCTS;       // no flow control

//tty.c_cflag     |=  CLOCAL;

//tty.c_cflag     |=  IGNBRK;
//tty.c_cflag     |=  IGNPAR;
//tty.c_cflag     |=  PARMRK;

tty.c_lflag     &=   ~PENDIN;          // no signaling chars, no echo, no canonical processing
tty.c_oflag     &=   ~OPOST;                  // no remapping, no delays
tty.c_cc[VMIN]      =  0;                  // read doesn't block
tty.c_cc[VTIME]     =  10;                  // 0.5 seconds read timeout

tty.c_cflag     |=  CREAD | CLOCAL;     // turn on READ & ignore ctrl lines
tty.c_iflag     |=  (IXON | IXOFF | IXANY);// turn off s/w flow ctrl
//tty.c_iflag     |=  /*OFILL |*/ ONLCR;//Ignorar caracter nulo
tty.c_lflag     &=  ~(ICANON | ECHO | ECHOE | ISIG); // make raw
tty.c_oflag     &=  ~OPOST;              // make raw

//Flush Port, then applies attributes 
tcflush( USB, TCIFLUSH );

if ( tcsetattr ( USB, TCSANOW, &tty ) != 0){
std::cout << "Error " << errno << " from tcsetattr" << std::endl;
}

/*AXIS.rI();
AXIS.rD();*/

while(run){
    
     // gettimeofday(&t1, NULL);
   



for(int i=0;i<sizeof bufc;i++)bufc[i]='\x00';
n = read( USB, &bufc , sizeof (bufc) );


//std::cout <<"RAW DATA="<<n<<"  "<<bufc<<std::endl;

if(bufc[0]=='A'/*&&bufc[3]=='\x04'*/){
    
    for(int i=0;i<sizeof RD;i++)RD[i]='\x00';
    
    for(int dp=0;dp<NBC-1;dp++){
        
        for(int i=0;i<sizeof bufc;i++)bufc[i]='\x00';
       n = read( USB, &bufc , sizeof (bufc) ); 
       
        RD[dp]=bufc[0];
        
    }
    
  //  std::cout /*<<RD*/<<std::endl;
    
     uint16_t value = RD[0] | uint16_t(RD[1]) << 8;
AXIS.A.S=(int)value;

NMEAS++;

/*std::cout <<bufc<<std::endl;*/
//std::cout <<(int)bufc[0]<<" "<<(int)bufc[1]<<" "<<(int)bufc[2]<<" "<<(int)bufc[3]<<std::endl;
//std::cout <<"DATA:"<<AXIS.A.S<<" NMEAS:"<<NMEAS<<std::endl;

//gettimeofday(&t2, NULL);

   // elapsedTime = (t2.tv_sec - t1.tv_sec) * 1000.0;      // sec to ms
   // elapsedTime += (t2.tv_usec - t1.tv_usec) / 1000.0;   // us to ms
   // std::cout << elapsedTime << " ms."<<std::endl;    
    
     //   gettimeofday(&t1, NULL);
    AXIS.uD();
    AXIS.wD();
}

}
//END RELATIVE EXECUTOR




close(USB);
}
