#include <iostream>
#include <SDL/SDL.h>
#include <SDL/SDL_image.h>
#include <GL/gl.h>
#include <GL/glu.h>
#include "camera.h"
#include <vector>

camera cam;
std::vector<std::vector<float> > heights;


unsigned int tex;


unsigned int loadTexture(const char* name)
{
	SDL_Surface* img=IMG_Load(name);
	SDL_PixelFormat form={NULL,32,4,0,0,0,0,8,8,8,8,0xff000000,0x00ff0000,0x0000ff00,0x000000ff,0,255};
//	std::cout << img->w << " " << img->h << std::endl;
	SDL_Surface* img2=SDL_ConvertSurface(img,&form,SDL_SWSURFACE);
	unsigned int texture;
	glGenTextures(1,&texture);
	glBindTexture(GL_TEXTURE_2D,texture);
	glTexImage2D(GL_TEXTURE_2D,0,GL_RGBA,img2->w,img2->h,0,GL_RGBA, GL_UNSIGNED_INT_8_8_8_8,img2->pixels);
	glTexParameteri(GL_TEXTURE_2D,GL_TEXTURE_MAG_FILTER,GL_LINEAR);
	glTexParameteri(GL_TEXTURE_2D,GL_TEXTURE_MIN_FILTER,GL_LINEAR);
	std::cout << glGetError() << std::endl;
	SDL_FreeSurface(img);
	SDL_FreeSurface(img2);
	return texture;
}


void loadHeightmap(const char* name)
{
	SDL_Surface* img=SDL_LoadBMP(name);
	if(!img)
	{
		std::cout << "image is not loaded" << std::endl;
		return;
	}
	std::vector<float> tmp;
	for(int i=0;i<img->h;i++)
	{
		tmp.clear();
		for(int j=0;j<img->w;j++)
		{
			Uint32 pixel=((Uint32*)img->pixels)[i*img->pitch/4 + j];
			Uint8 r,g,b;	//unsigned char
			SDL_GetRGB(pixel,img->format,&r,&g,&b);
			tmp.push_back((float)r/255.0);	//0.0,1.0
		}
		heights.push_back(tmp);
	}
}

void renderHeightmap(float size,float h)
{
	glEnable(GL_TEXTURE_2D);
	glBindTexture(GL_TEXTURE_2D,tex);
	for(int i=0;i<heights.size()-1;i++)
		for(int j=0;j<heights[0].size()-1;j++)
		{
			glBegin(GL_TRIANGLE_STRIP);
				glTexCoord2f(i*1./heights.size(),j*1./heights[0].size());
				glVertex3f(i*size,heights[i][j]*h,j*size);
				glTexCoord2f((i+1)*1./heights.size(),j*1./heights[0].size());
				glVertex3f((i+1)*size,heights[i+1][j]*h,j*size);
				glTexCoord2f(i*1./heights.size(),(j+1)*1./heights[0].size());
				glVertex3f(i*size,heights[i][j+1]*h,(j+1)*size);
				glTexCoord2f((i+1)*1./heights.size(),(j+1)*1./heights[0].size());
				glVertex3f((i+1)*size,heights[i+1][j+1]*h,(j+1)*size);
			glEnd();
		}
	glDisable(GL_TEXTURE_2D);
}

void init(float angle)
{
	glClearColor(0,0,0,1);
	glMatrixMode(GL_PROJECTION);
		glLoadIdentity();
		gluPerspective(angle,640.0/480.0,1,1000);
	glMatrixMode(GL_MODELVIEW);
	glEnable(GL_DEPTH_TEST);
	loadHeightmap("heightmap2.bmp");
	tex=loadTexture("xyz.png");
}

void display()
{
	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
	glLoadIdentity();
	cam.Control();
	cam.UpdateCamera();
	renderHeightmap(0.5,16);
	
}



int main()
{
	SDL_Init(SDL_INIT_EVERYTHING);
	SDL_SetVideoMode(640,480,32,SDL_OPENGL);
	Uint32 lastTime;
	SDL_Event event;
	bool running=true;
	float angle=50;
	init(angle);
	bool b=false;
	int numFrames=0;
	while(running)
	{
		while(SDL_PollEvent(&event))
		{
			switch(event.type)
			{
				case SDL_QUIT:
					running=false;
					break;
				case SDL_KEYDOWN:
					switch(event.key.keysym.sym)
					{
						case SDLK_ESCAPE:
							running=false;
							break;
					}
					break;
				case SDL_MOUSEBUTTONDOWN:
					cam.mouseIn(true);
					break;
					
			}
		}
		display();
		SDL_GL_SwapBuffers();
		Uint32 elapsed=SDL_GetTicks()-lastTime;
		numFrames++;
		if(elapsed>=1000)
		{
			lastTime=SDL_GetTicks();
			std::cout << (float)numFrames/((float)elapsed/1000.0) << std::endl;
			numFrames=0;
		}
		
	//	if(1000.0/30>SDL_GetTicks()-start)
	//		SDL_Delay(1000.0/30-(SDL_GetTicks()-start));
	}
	SDL_Quit();
}
