#ifndef GRAPH_H_INCLUDED
#define GRAPH_H_INCLUDED

class GRAPH{
    
    public:

std::vector <float> X;
std::vector <float> Y;
std::vector <std::vector<float>> Z;

float Xmin=-300;
float Xmax=300;
float Xdom=600;//Espacio en el que representar el gráfico

float Ymin=-150;
float Ymax=150;
float Ydom=300;//Espacio en el que representar el gráfico

float Zmin=0;
float Zmax=0;
float Zdom=0;//Espacio en el que representar el gráfico

bool lines=true;
bool surf=false;



	float color[3]={0,0,0};

	GRAPH() {
		lines=true;
	}

	~GRAPH() {
    X.clear();
    Y.clear();
    Z.clear();
	}

	void DU(float variable) {//Actualización dinamica de graficos, almacena datos de variable en orden.
Y[Y.size()-1]=variable;
for(int n=1;n<Y.size();n++){
    Y[n-1]=Y[n];
}
	}
	
		void DU(float Xv, float Yv) {//Actualización dinamica de graficos, almacena datos de variable en orden.
Y[Y.size()-1]=Yv;
for(int n=1;n<Y.size();n++){
    Y[n-1]=Y[n];
}
	}

	void D() {//Rutinas de dibujado del grafico

		glLineWidth(2);
		glColor3f(color[0], color[1], color[2]);
                
		glBegin(GL_LINE_STRIP);

		//for(int i=0;i<sizeof(ypoints);i+=width/sizeof(ypoints)){
                /*if(Z[0].size()>0){//Gráfico tridimensional
		for (int y = 0; y<Y.size(); y++) {
                    
                    for (int x=0; x<X.size();x++){
				glVertex3f(X[x], Y[y],Z[x][y]);
                    }

		}}*/
		//else{ //Gráfico bidimensional
                    for (int x=0;x<X.size();x++){
                        glVertex3f(X[x], Y[x],0);
                        
                    }
                    
               // }

		glEnd();
                		glColor3f(1,1,1);
	}
};


#endif
