#ifndef GEO_2D_H_INCLUDED
#define GEO_2D_H_INCLUDED

void DRect(float sizex, float sizey) {
	glBegin(GL_QUADS);
	glVertex2f(0.0, 0.0);
	glVertex2f(sizex, 0.0);
	glVertex2f(sizex, sizey);
	glVertex2f(0.0, sizey);
	glColor3f(1.0, 1.0, 1.0);
	glEnd();
}

class MATRIX_2D {
	int W = 0; int H = 0;

	int PIX = 0;
	int bpp = 0;

	std::vector <int> MATRIXDATA;

	int mxpos = 0;
	bool last = false;


public:

	//UINT64 state = 0;
	//UINT64 total = 0;

	//UINT64 cicles = 0;

	MATRIX_2D (int Wv, int Hv, int bppv){
		W = Wv; H = Hv; bpp = bppv;
		PIX = W*H;// total = pow(bpp * 2, PIX);
		MATRIXDATA.assign(PIX*bpp,0);
		for (int r = 0; r < W;r++) MATRIXDATA[r] = rand() % 2;

	}
	~MATRIX_2D() {
		MATRIXDATA.clear();
	}

	/*void UMATIX() {

			for (int s = 1; s < (PIX*bpp) - 1; s++) {
				if (state % s == 0)MATRIXDATA[s] = 1;
				else MATRIXDATA[s] = 0;

			}

			state++; 
			//std::cout << state << "  " << total << "  " << cicles<< std::endl;
			if (state >= (pow(bpp * 2, PIX))) { state = 0; cicles++; }
		}*/

	void UMATRIX_pro() {
		mxpos = 0;
		last = true;

		while (last) {

			if (MATRIXDATA[mxpos] == 0) {
				MATRIXDATA[mxpos] = 1;
				for (int e = mxpos-1; e > 0; e--) { MATRIXDATA[e] = 0; }
				last = false;
			}
			mxpos++;
			if (mxpos >= PIX) { mxpos = 0; last = false; for (int rese = 0; rese < PIX; rese++) { MATRIXDATA[rese] = 0; }
			}
			//std::cout << mxpos << "  " <<last<< std::endl;
		}
	}
	

	void DMATRIX() {
		glBegin(GL_POINTS);
		glPointSize(0.5);
		//glLineWidth(2);
		//glVertex2f(0.0, 0.0);
		for (int y = 0; y < H; y++) {		
		for (int x = 0; x < W; x++) {
			glColor3f(GLfloat(MATRIXDATA[(x+(y*H))]), GLfloat(MATRIXDATA[(x + (y*H))]), GLfloat(MATRIXDATA[(x + (y*H))]));
			//glColor3f(y, x, x*y);
			glVertex2f(x, y);
		}
		}
		//glVertex2f(H, W);
		glEnd();
		glColor3f(1.0, 1.0, 1.0);
	}
};





struct point {
	GLfloat x;
	GLfloat y;
};








/*class GRAPH_2D {
	//GLfloat x;
	//GLfloat y;

	std::vector <GLfloat> xpoints;
	std::vector <GLfloat> ypoints;

	int width = 0;

	point graph[2000];
	GLuint vbo;

public:
	GRAPH_2D(int w) {*/
		/*width = w;
		xpoints.assign(width, 0);
		ypoints.assign(width, 0);

		GLuint vbo;

		glGenBuffers(1, &vbo);
		glBindBuffer(GL_ARRAY_BUFFER, vbo);

		glBufferData(GL_ARRAY_BUFFER, width, graph, GL_STATIC_DRAW);*/
	

		/*glGenBuffers(1, &vbo);
		glBindBuffer(GL_ARRAY_BUFFER, vbo);
		

		for (int i = 0; i < 2000; i++) {
			float x = (i - 1000.0) / 100.0;
			graph[i].x = x;
			graph[i].y = sin(x * 10.0) / (1.0 + x * x);

			glBufferData(GL_ARRAY_BUFFER, sizeof graph, graph, GL_STATIC_DRAW);

			glBindBuffer(GL_ARRAY_BUFFER, vbo);*/

//			glEnableVertexAttribArray(attribute_coord2d);
		/*	glVertexAttribPointer(
			//	attribute_coord2d,   // attribute
				2,                   // number of elements per vertex, here (x,y)
				GL_FLOAT,            // the type of each element
				GL_FALSE,            // take our values as-is
				0,                   // no space between values
				0                    // use the vertex buffer object
				);*/
		/*}
	}

	void DG_2D(){*/
		/*for (int i = 0; i < width; i++) {
			float x = (i - 1000.0) / 100.0;
			 xpoints[i]= x;
			ypoints [i]= sin(x * 10.0) / (1.0 + x * x);
		}*/
		/*glDrawArrays(GL_LINE_STRIP, 0, 2000);

	//	glDisableVertexAttribArray(attribute_coord2d);
		glBindBuffer(GL_ARRAY_BUFFER, 0);
	}
};*/

#endif
