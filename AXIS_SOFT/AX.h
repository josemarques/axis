#ifndef AX_H_INCLUDED
#define AX_H_INCLUDED

#include <math.h>       /* sqrt */

#include "AXGL.h"

#include "../LIB/LINUX/DPR.h"
#include "../LIB/LINUX/SML.h"
#include "../LIB/LINUX/SNC.h"

//Las unidades b�sicas de sistema son nanometros, minutos, nanonewtons y nanowatios. usando enteros

class AXA{//Clase de actuador

public:
long A=0;//Valor comun
long AI=0;//Valor de la instrucción
long AS=0;//Valor de inicio de la instrucción
long ABI=0;//Valor de la anterior instrucción
double AR=0;//Valor de relatividad
bool EN=false;//Actividad de actuador 
};

class AXS{//Clase de sensor

public:
int S=0;
};

class AXNF{//Clase de datos e intrucciones inferidos
public:
double N;//Valor comun
double NI;//Valor de instruccion
double NS;//Valor de inicio de instruccion
double NBI;//Valor de instruccion previa
double NR;//Valor de relatividad
};


class AXH{//Clase AXIS CNC, conjunto de clases anteriores, 
public:
    
std::string AXd;//String de datos actuales
std::string AXi;//String de intrucciones actuales



long MDEL=5;//Master delay, retraso que regula el sistema en microsegundos.

SM CD;//Memoria compartida de datos actuales
SM CI;//Memoria compartida de instrucciones actuales

SNC IS;//Semaforo de sincronizacion de instrucciones

//Constantes de funcionamiento
const int STEP=400;//400 pasos/revolucion
const int AXPAS=1000000/STEP;//nm/paso

const float SECFR=80;//Velocidad de avance segura mm/min
const float SECAR=2400;//Aceleraci�n y desaceleraci�n segura mm/min^2

AXA X;//Actuador lineal X nanometros
AXA Y;//Actuador lineal Y nanometros
AXA Z;//Actuador lineal Z nanometros

AXA S;//Actuador motor de corte RPM

AXNF F;//Velocidad de avance nanometos/min
AXNF A;//Aceleraci�n y deceleraci�n de avance nanometros/min^2
CRN CRONv;
CRN CRONa;
int CRNc=0;//Cuenta el numero de instruciones ejecutadas.
double DE0=0; //Distancia euclidea para c�lculo de velocidad
double DE1=0; //Distancia euclidea para c�lculo de velocidad
double VE0=0; //Velocidad euclidea para c�lculo de aceleracion
double VE1=0; //Velocidad euclidea para c�lculo de aceleracion

//AXNF M;//Par de corte
//AXNF N;//Potencia de corte

bool RES=false;//Reset signal
bool EME=false;//Emergency signal



AXH(){
    //CD=SM(10255,1024);
    //CI=SM(8000,1024);
    CD=SM(10255,2048);
    CI=SM(6000,2048);
    
    IS=SNC(5800,1);
  
//A.NI=SECAR;
//F.NI=SECFR;
/*F.NI=SECFR;
F.N=SECFR;

A.NI=SECAR;
A.N=SECAR;*/


/*//Lee los datos actuales de la memoria compartida
AXd.clear();
CD.R();
AXd=CD.AXSM;

X.A=GETGDR("X",AXd,X.A);
Y.A=GETGDR("Y",AXd,Y.A);
Z.A=GETGDR("Z",AXd,Z.A);

S.A=GETGDR("S",AXd,S.A);

F.N=GETGDR("T",AXd,F.N);
A.N=GETGDR("A",AXd,A.N);

//Lee las instruciones actuales de la memoria compartida
AXi.clear();
CI.R();
AXi+=CI.AXSM;

X.AI=GETGDR("X",AXi,X.AI);
Y.AI=GETGDR("Y",AXi,Y.AI);
Z.AI=GETGDR("Z",AXi,Z.AI);

S.AI=GETGDR("S",AXi,S.AI);

F.NI=GETGDR("T",AXi,F.NI);
A.NI=GETGDR("A",AXi,A.NI);*/

}

double AXRELF(float s,float r,float i){//Rutina de calculo de relatividad entre instrucciones //Ejecucion continua
    double S=s,R=r,I=i;
if(abs(S-I)==0) return 1;
else{
double TR=abs(S-R);
double TRI=abs(S-I);
double RE=TR/TRI;
 return RE;}
}

double DSt(AXA D){//Distancia entrte inicio y final de instrucion.
   return( abs(D.AS-D.AI));
}

double DSt(AXNF D){//Distancia entrte inicio y final de instrucion.
   return( abs(D.NS-D.NI));
}

double DSr(AXA D){//Distancia actual o real hasta fin de instruccion
    return( abs(D.A-D.AI));
}

double DSr(AXNF D){//Distancia actual o real hasta fin de instruccion
    return( abs(D.N-D.NI));
}

bool I(){//Determina si el sistema esta esperando una instruccion"true" o no"false"
    if(X.A==X.AI&&Y.A==Y.AI&&Z.A==Z.AI){
        return true;
    }
    else return false;
}

bool cI(){//determina si la intrucci�on ha cambiado"true" o no"false"
     if(X.ABI!=X.AI||Y.ABI!=Y.AI||Z.ABI!=Z.AI||S.ABI!=S.AI||F.NBI!=F.NI||A.NBI!=A.NI){//Si la instrucci�n ha cambiado
     return true;         
    }
    else return false;
}

/*float pI(){//Devuelve el porcentage para la finalizaci�n de la instrucci�n
    return((float)((DSr(X)+DSr(Y)+DSr(Z))/(DSt(X)+DSt(Y)+DSt(Z))));
}*/
void VC(){//Obtiene el tiempo de pausa entre instruccionesw para obtener la velocidad buscadan usando la velocidad actual basada en la aceleracion, no la de instrucci�n
            MDEL=(int)((((AXPAS*1e-6)/(F.N))-(7.507507507e-6))/(18e-9)); 
    if(MDEL<=0){MDEL=0;}
}

void AVC(){//rutinas de c�lculo de velocidad y aceleracion, ejecutar una vez por ciclo
    if(CRNc==1){
        CRONv.STP();
        CRONv.LPS();
        
    }
    
    else if(CRNc==2){
        
        CRONa.STRT();
    }
    
    else if(CRNc==3){
        
        CRONa.STRT();
        CRNc=0;
    }
    
    else if(CRNc==0){
        CRONv.STRT();
        CRONa.STRT();
    }
    
    
    CRNc+=1;//Cuenta el numero de instruciones ejecutadas.
}

void uDS(){//Mete las variables a AXd
//AXd.clear()
AXd=ADDGDS(AXd,"X",X.A);
AXd=ADDGDS(AXd,"Y",Y.A);
AXd=ADDGDS(AXd,"Z",Z.A);

AXd=ADDGDS(AXd,"S",S.A);

AXd=ADDGDS(AXd,"T",F.N);
AXd=ADDGDS(AXd,"A",A.N);
}

void uD(){//Lee AXd y actualiza las variables
X.A=GETGDR("X",AXd,X.A);
Y.A=GETGDR("Y",AXd,Y.A);
Z.A=GETGDR("Z",AXd,Z.A);

S.A=GETGDR("S",AXd,S.A);

F.N=GETGDR("T",AXd,F.N);
A.N=GETGDR("A",AXd,A.N);
}

void rD(){//Lee los datos de la MC y los mete al string
    AXd.clear();
    CD.R();
    AXd=CD.AXSM;
}

void wD(){//Escribe los datos AXd en la memoria compartida
CD.W(AXd);
}


void uIS(){//Mete las variables a AXi
    //AXi.clear(); Comentado para no eliminar informacion proporcionada por otros programas
    AXi=ADDGDS(AXi,"X",X.AI);
    AXi=ADDGDS(AXi,"Y",Y.AI);
    AXi=ADDGDS(AXi,"Z",Z.AI);

    if(S.AI<0)S.AI=0;
    AXi=ADDGDS(AXi,"S",S.AI);
    
    if(F.NI<=0)F.NI=1;
    AXi=ADDGDS(AXi,"T",F.NI);
    if(A.NI<=1)A.NI=1;
    AXi=ADDGDS(AXi,"A",A.NI);
}

void uI(){//Lee AXi y actualiza las variables
    
    if(GETC("stop",AXi)){//Emergencia habilitada
    EME=true;
    X.AI=X.A;
    Y.AI=Y.A;
    Z.AI=Z.A;
    S.AI=0;
    }   
    else if(GETC("start",AXi)){//Emergencia deshabilitada
    EME=false;
    }
    else{
    X.ABI=X.AI;
    X.AI=GETGDR("X",AXi,X.AI);
    Y.ABI=Y.AI;
    Y.AI=GETGDR("Y",AXi,Y.AI);
    Z.ABI=Z.AI;
    Z.AI=GETGDR("Z",AXi,Z.AI);
    
    S.ABI=S.AI;
    S.AI=GETGDR("S",AXi,S.AI);
    if(S.AI<0)S.AI=0;
    
    F.NBI=F.NI;
    F.NI=GETGDR("T",AXi,F.NI);
    if(F.NI<=0)F.NI=1;
    
    A.NBI=A.NI;
    A.NI=GETGDR("A",AXi,A.NI);
    if(A.NI<=0)A.NI=1;
    
    //Calculo y movimiento de la velocidad y aceleracion
    
    //Claculo y movimiento de la velocidad y aceleracion
    
    if(cI()){//Si la instrucci�n ha cambiado
        X.AS=X.A;
        Y.AS=Y.A;
        Z.AS=Z.A;
        
        // X.AR=AXRELF(X.AS,X.A,X.AI);
        // Y.AR=AXRELF(Y.AS,Y.A,Y.AI);
        // Z.AR=AXRELF(Z.AS,Z.A,Z.AI);
    }
    }

}


void rI(){//Lee las instrucciones de la memoria comparida y las mete a las variables de AXH
AXi.clear();
CI.R();
AXi=CI.AXSM;
}

void wI(){//Escribe las instrucciones a la memoria compartida
CI.W(AXi);
}




void WSS(){//Escribe el archivo de estado salvado
  /*rI();rD();
    std::ofstream SS;
  SS.open ("SS.gs");
  SS << AXd<<std::endl<<AXi<<std::endl<<CPD;
  SS.close();*/
}

void RSS(){//Lee el archivo de estado guardado
    
  /*  std::ifstream SS;
std::string line=" ";
int lineID=0;

AXd.clear();
AXi.clear();

SS.open ("SS.gs", std::ios::in|std::ios::ate);
  if (SS.is_open())  {
      
   while (!SS.eof()){
       
       getline(SS, line);
       
       if(lineID==0){
           AXd=line;
    }
    
    else if(lineID==1){
        AXi=line;
    }
    
    else if(lineID==2){
        
        CPD=line;
    }
    
    lineID++;
   }

    SS.close();
    
    lineID=0;
}


wI();

wD();*/

}


};

#endif
