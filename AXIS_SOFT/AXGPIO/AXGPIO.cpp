//
//  How to access GPIO registers from C-code on the Raspberry-Pi
//  Example program
//  15-January-2012
//  Dom and Gert
//  Revised: 15-Feb-2013

//  Revised: 31-Mar-2013 for SVLUG RPi Presentation

// Access from ARM Running Linux

//#define BCM2708_PERI_BASE        0x20000000//For other raspberris
#define BCM2708_PERI_BASE        0x3F000000//For rpi 2 y 3
#define GPIO_BASE                (BCM2708_PERI_BASE + 0x200000) /* GPIO controller */


#include <stdio.h>
#include <stdlib.h>
#include <fcntl.h>
#include <sys/mman.h>
#include <unistd.h>
#include <signal.h>
#include <time.h>

#include "../AX.h"

#define PAGE_SIZE (4*1024)
#define BLOCK_SIZE (4*1024)

#define XS 2
#define XRS 3
#define XE 4

#define YS 5
#define YRS 6
#define YE 7

#define ZS 9
#define ZRS 10
#define ZE 11

#define VS 8
#define VRS 12

#define US 13
#define URS 14
#define UE 15

#define SS 16
#define SRS 17
#define SE 18

int  mem_fd;
void *gpio_map;

// I/O access
volatile unsigned *gpio;


// GPIO setup macros. Always use INP_GPIO(x) before using OUT_GPIO(x) or SET_GPIO_ALT(x,y)
#define INP_GPIO(g) *(gpio+((g)/10)) &= ~(7<<(((g)%10)*3))
#define OUT_GPIO(g) *(gpio+((g)/10)) |=  (1<<(((g)%10)*3))
#define SET_GPIO_ALT(g,a) *(gpio+(((g)/10))) |= (((a)<=3?(a)+4:(a)==4?3:2)<<(((g)%10)*3))

#define GPIO_SET *(gpio+7)  // sets   bits which are 1 ignores bits which are 0
#define GPIO_CLR *(gpio+10) // clears bits which are 1 ignores bits which are 0

void setup_io();
void signalHandler(int);

int main(int argc, char **argv)
{
  // Set up gpi pointer for direct register access
  setup_io();

  signal(SIGINT, signalHandler);

  std::cout<<"AXGPIO V0.41 C++"<<std::endl;
  

// Set GPIO pin to output 
  
INP_GPIO(XS); // must use INP_GPIO before we can use OUT_GPIO
OUT_GPIO(XS);

INP_GPIO(XRS); // must use INP_GPIO before we can use OUT_GPIO
OUT_GPIO(XRS);

INP_GPIO(XE); // must use INP_GPIO before we can use OUT_GPIO
OUT_GPIO(XE);

INP_GPIO(YS); // must use INP_GPIO before we can use OUT_GPIO
OUT_GPIO(YS);

INP_GPIO(YRS); // must use INP_GPIO before we can use OUT_GPIO
OUT_GPIO(YRS);

INP_GPIO(YE); // must use INP_GPIO before we can use OUT_GPIO
OUT_GPIO(YE);

INP_GPIO(ZS); // must use INP_GPIO before we can use OUT_GPIO
OUT_GPIO(ZS);

INP_GPIO(ZRS); // must use INP_GPIO before we can use OUT_GPIO
OUT_GPIO(ZRS);

INP_GPIO(ZE); // must use INP_GPIO before we can use OUT_GPIO
OUT_GPIO(ZE);

INP_GPIO(US); // must use INP_GPIO before we can use OUT_GPIO
OUT_GPIO(US);

INP_GPIO(URS); // must use INP_GPIO before we can use OUT_GPIO
OUT_GPIO(URS);

INP_GPIO(UE); // must use INP_GPIO before we can use OUT_GPIO
OUT_GPIO(UE);

INP_GPIO(VS); // must use INP_GPIO before we can use OUT_GPIO
OUT_GPIO(VS);

INP_GPIO(VRS); // must use INP_GPIO before we can use OUT_GPIO
OUT_GPIO(VRS);


INP_GPIO(SS); // must use INP_GPIO before we can use OUT_GPIO
OUT_GPIO(SS);

INP_GPIO(SRS); // must use INP_GPIO before we can use OUT_GPIO
OUT_GPIO(SRS);

INP_GPIO(SE); // must use INP_GPIO before we can use OUT_GPIO
OUT_GPIO(SE);

// Set GPIO pin to output 

//AXIS INITIALIZER ROUTINES
const int AXPAS=1000000/400;//nanometros a x nanometr/paso
const int IDEL=1;//Delay de impulso

bool run=true;

AXH AXIS;

AXIS.rI();
AXIS.uI();

AXIS.rD();
AXIS.uD();

int xd=1;
int yd=1;
int zd=1;

double cx=1e100;
double cy=2e100;
double cz=3e100;

long nS=0, nC=0, aF=0, dS=0;

AXIS.A.NI=AXIS.SECAR;
AXIS.F.NI=AXIS.SECFR;

if(true){//Si la instrucci�n cambia calcula la relatividad e inicializa los sistemas electronicos
    
    //std::cout<<"ICHNG"<<std::endl;
    
    //Set spindle inst
    if(AXIS.S.AI==AXIS.S.A){
        
    }
    else if(AXIS.S.AI==0){
    GPIO_CLR = 1<<SE;//0
    usleep(1000*IDEL);
    AXIS.S.A=0;
    }
    else {
        GPIO_SET = 1<<SE;//1
        usleep(1000*IDEL);
        if(AXIS.S.AI>AXIS.S.A){
            for(int i=0;AXIS.S.AI>AXIS.S.A;i++) {
                usleep(1000*IDEL);
                GPIO_SET = 1<<SS;//1
                usleep(1000*IDEL);
                GPIO_CLR = 1<<SS;//0
                AXIS.S.A+=1;
            }
        }
        else if(AXIS.S.AI<AXIS.S.A){
            for(int i=0;AXIS.S.AI<AXIS.S.A;i++) {
                usleep(1000*IDEL);
                GPIO_SET = 1<<SRS;//1
                usleep(1000*IDEL);
                GPIO_CLR = 1<<SRS;//0
                AXIS.S.A-=1;
                
            }
        }
    }
    //Set spindle inst
    
    
    if(AXIS.DSt(AXIS.X)>=AXIS.DSt(AXIS.Y)&&AXIS.DSt(AXIS.X)>=AXIS.DSt(AXIS.Z)){//Obtiene la instrucci�n que m�s tardar� en ejecutarse
        AXIS.X.AR=1;
        if(AXIS.DSt(AXIS.Y)!=0)AXIS.Y.AR=(double)AXIS.DSt(AXIS.X)/(double)AXIS.DSt(AXIS.Y);
        else AXIS.Y.AR=1e100;
        if(AXIS.DSt(AXIS.Z)!=0)AXIS.Z.AR=(double)AXIS.DSt(AXIS.X)/(double)AXIS.DSt(AXIS.Z);
        else AXIS.Z.AR=1e100;
    }
    else if(AXIS.DSt(AXIS.Y)>=AXIS.DSt(AXIS.X)&&AXIS.DSt(AXIS.Y)>=AXIS.DSt(AXIS.Z)){
        AXIS.Y.AR=1;
        if(AXIS.DSt(AXIS.X)!=0) AXIS.X.AR=(double)AXIS.DSt(AXIS.Y)/(double)AXIS.DSt(AXIS.X);
        else AXIS.X.AR=1e100;
        if(AXIS.DSt(AXIS.Z)!=0) AXIS.Z.AR=(double)AXIS.DSt(AXIS.Y)/(double)AXIS.DSt(AXIS.Z);
        else AXIS.Z.AR=1e100;
    }
    else if(AXIS.DSt(AXIS.Z)>=AXIS.DSt(AXIS.X)&&AXIS.DSt(AXIS.Z)>=AXIS.DSt(AXIS.Y)){
        AXIS.Z.AR=1;
        if(AXIS.DSt(AXIS.X)!=0)AXIS.X.AR=(double)AXIS.DSt(AXIS.Z)/(double)AXIS.DSt(AXIS.X);
        else AXIS.X.AR=1e100;
        if(AXIS.DSt(AXIS.Y)!=0)AXIS.Y.AR=(double)AXIS.DSt(AXIS.Z)/(double)AXIS.DSt(AXIS.Y);
        else AXIS.Y.AR=1e100;
    }
    
        cx=AXIS.X.AR;
        cy=AXIS.Y.AR;
        cz=AXIS.Z.AR;//*/
        
    //Inicializa los sistemas electrt�nicos
    //Enable
    if(AXIS.X.AI!=AXIS.X.A) GPIO_SET = 1<<XE;//1
    else GPIO_CLR = 1<<XE;//0
    
    if(AXIS.Y.AI!=AXIS.Y.A) GPIO_SET = 1<<YE;//1
    else GPIO_CLR = 1<<YE;//0
    
    if(AXIS.Z.AI!=AXIS.Z.A) GPIO_SET = 1<<ZE;//1
    else GPIO_CLR = 1<<ZE;//0
    
    //Direccion
    if(AXIS.X.AI<AXIS.X.A) {
        GPIO_SET = 1<<XRS;//1
        xd=-1;
    }
    else{ 
        GPIO_CLR = 1<<XRS;//0
        xd=1;
    }
    
    if(AXIS.Y.AI<AXIS.Y.A){
        GPIO_SET = 1<<YRS;//1
        yd=-1;
    }
    else {
        GPIO_CLR = 1<<YRS;//0
        yd=1;
    }
    
    if(AXIS.Z.AI<AXIS.Z.A) {
        GPIO_SET = 1<<ZRS;//1
        zd=-1;
    }
    else {
        GPIO_CLR = 1<<ZRS;//0
        zd=1;
    }
    
    //Speed and aceleration initialization
    /*AXIS.MDEL=(int)((((AXPAS*1e-6)/AXIS.F.NI)-(7.507507507e-6))/(18e-9)); 
    if(AXIS.MDEL<=0){AXIS.MDEL=0;}*/
    
    nS=(int)((AXIS.DSt(AXIS.X)+AXIS.DSt(AXIS.Y)+AXIS.DSt(AXIS.Z))/(AXPAS));//Calcula el numero de pasos totales    
    aF=(int)(((AXIS.F.NI*AXIS.F.NI)/(2*AXIS.A.NI))/(AXPAS*1e-6));//Calcula los pasos necesarios para acelerar
    if(aF>=(nS/2)){aF=int(nS/2);}
    dS=nS-aF;//Determina el paso apartir del cual desacelerar
    nC=0;//Numero de pasos realizados
    //AXIS.A.NR=sqrt(2*AXIS.A.NI*AXPAS*1e-3);//Aceleracion relativa entre pasos
    AXIS.F.NR=sqrt(2*AXIS.A.NI*AXIS.AXPAS*1e-6*aF);//Velocidad m�xima alcanzada
    /*//DBG
        std::cout<<"A.N: "<<AXIS.A.N<<std::endl;
        std::cout<<"A.NI: "<<AXIS.A.NI<<std::endl;        
        std::cout<<"nS: "<<nS<<std::endl;
        std::cout<<"aF: "<<aF<<std::endl;
        std::cout<<"dS: "<<dS<<std::endl;
        std::cout<<"F.NR: "<<AXIS.F.NR<<std::endl;
    //DBG*/
    //Speed and aceleration initialization
    
    /*//DBG
    std::cout<<"xd: "<<xd<<std::endl;
    std::cout<<"yd: "<<yd<<std::endl;
    std::cout<<"zd: "<<zd<<std::endl;
    
    std::cout<<"XREL: "<<AXIS.X.AR<<std::endl;
    std::cout<<"YREL: "<<AXIS.Y.AR<<std::endl;
    std::cout<<"ZREL: "<<AXIS.Z.AR<<std::endl;
    
    std::cout<<"AXMDEL: "<<AXIS.MDEL<<std::endl;
    std::cout<<"AXIS.F.NI: "<<AXIS.F.NI<<std::endl;
    //DBG*/
    
    usleep(IDEL);//Calentando electronica, estabilizaci�n segura de se�al.
    
}




while(run){

AXIS.IS.SNCR(0);//Se bloquea hasta que el semaforo esté en 0 

AXIS.rI();
AXIS.uI();

if(AXIS.cI()){//Si la instrucci�n cambia calcula la relatividad e inicializa los sistemas electronicos
    
    //std::cout<<"ICHNG"<<std::endl;
    
    //Set spindle inst
    if(AXIS.S.AI==AXIS.S.A){
        
    }
    else if(AXIS.S.AI==0){
    GPIO_CLR = 1<<SE;//0
    usleep(1000*IDEL);
    AXIS.S.A=0;
    }
    else {
        GPIO_SET = 1<<SE;//1
        usleep(1000*IDEL);
        if(AXIS.S.AI>AXIS.S.A){
            for(int i=0;AXIS.S.AI>AXIS.S.A;i++) {
                usleep(1000*IDEL);
                GPIO_SET = 1<<SS;//1
                usleep(1000*IDEL);
                GPIO_CLR = 1<<SS;//0
                AXIS.S.A+=1;
            }
        }
        else if(AXIS.S.AI<AXIS.S.A){
            for(int i=0;AXIS.S.AI<AXIS.S.A;i++) {
                usleep(1000*IDEL);
                GPIO_SET = 1<<SRS;//1
                usleep(1000*IDEL);
                GPIO_CLR = 1<<SRS;//0
                AXIS.S.A-=1;
                
            }
        }
    }
    //Set spindle inst
    
    
    if(AXIS.DSt(AXIS.X)>=AXIS.DSt(AXIS.Y)&&AXIS.DSt(AXIS.X)>=AXIS.DSt(AXIS.Z)){//Obtiene la instrucci�n que m�s tardar� en ejecutarse
        AXIS.X.AR=1;
        if(AXIS.DSt(AXIS.Y)!=0)AXIS.Y.AR=(double)AXIS.DSt(AXIS.X)/(double)AXIS.DSt(AXIS.Y);
        else AXIS.Y.AR=1e100;
        if(AXIS.DSt(AXIS.Z)!=0)AXIS.Z.AR=(double)AXIS.DSt(AXIS.X)/(double)AXIS.DSt(AXIS.Z);
        else AXIS.Z.AR=1e100;
    }
    else if(AXIS.DSt(AXIS.Y)>=AXIS.DSt(AXIS.X)&&AXIS.DSt(AXIS.Y)>=AXIS.DSt(AXIS.Z)){
        AXIS.Y.AR=1;
        if(AXIS.DSt(AXIS.X)!=0) AXIS.X.AR=(double)AXIS.DSt(AXIS.Y)/(double)AXIS.DSt(AXIS.X);
        else AXIS.X.AR=1e100;
        if(AXIS.DSt(AXIS.Z)!=0) AXIS.Z.AR=(double)AXIS.DSt(AXIS.Y)/(double)AXIS.DSt(AXIS.Z);
        else AXIS.Z.AR=1e100;
    }
    else if(AXIS.DSt(AXIS.Z)>=AXIS.DSt(AXIS.X)&&AXIS.DSt(AXIS.Z)>=AXIS.DSt(AXIS.Y)){
        AXIS.Z.AR=1;
        if(AXIS.DSt(AXIS.X)!=0)AXIS.X.AR=(double)AXIS.DSt(AXIS.Z)/(double)AXIS.DSt(AXIS.X);
        else AXIS.X.AR=1e100;
        if(AXIS.DSt(AXIS.Y)!=0)AXIS.Y.AR=(double)AXIS.DSt(AXIS.Z)/(double)AXIS.DSt(AXIS.Y);
        else AXIS.Y.AR=1e100;
    }
    
        cx=AXIS.X.AR;
        cy=AXIS.Y.AR;
        cz=AXIS.Z.AR;//*/
        
    //Inicializa los sistemas electrt�nicos
    //Enable
    if(AXIS.X.AI!=AXIS.X.A) GPIO_SET = 1<<XE;//1
    else GPIO_CLR = 1<<XE;//0
    
    if(AXIS.Y.AI!=AXIS.Y.A) GPIO_SET = 1<<YE;//1
    else GPIO_CLR = 1<<YE;//0
    
    if(AXIS.Z.AI!=AXIS.Z.A) GPIO_SET = 1<<ZE;//1
    else GPIO_CLR = 1<<ZE;//0
    
    //Direccion
    if(AXIS.X.AI<AXIS.X.A) {
        GPIO_SET = 1<<XRS;//1
        xd=-1;
    }
    else{ 
        GPIO_CLR = 1<<XRS;//0
        xd=1;
    }
    
    if(AXIS.Y.AI<AXIS.Y.A){
        GPIO_SET = 1<<YRS;//1
        yd=-1;
    }
    else {
        GPIO_CLR = 1<<YRS;//0
        yd=1;
    }
    
    if(AXIS.Z.AI<AXIS.Z.A) {
        GPIO_SET = 1<<ZRS;//1
        zd=-1;
    }
    else {
        GPIO_CLR = 1<<ZRS;//0
        zd=1;
    }
    
    //Speed and aceleration initialization
    /*AXIS.MDEL=(int)((((AXPAS*1e-6)/AXIS.F.NI)-(7.507507507e-6))/(18e-9)); 
    if(AXIS.MDEL<=0){AXIS.MDEL=0;}*/
    
    nS=(int)((AXIS.DSt(AXIS.X)+AXIS.DSt(AXIS.Y)+AXIS.DSt(AXIS.Z))/(AXPAS));//Calcula el numero de pasos totales    
    aF=(int)(((AXIS.F.NI*AXIS.F.NI)/(2*AXIS.A.NI))/(AXPAS*1e-6));//Calcula los pasos necesarios para acelerar
    if(aF>=(nS/2)){aF=int(nS/2);}
    dS=nS-aF;//Determina el paso apartir del cual desacelerar
    nC=0;//Numero de pasos realizados
    //AXIS.A.NR=sqrt(2*AXIS.A.NI*AXPAS*1e-3);//Aceleracion relativa entre pasos
    AXIS.F.NR=sqrt(2*AXIS.A.NI*AXIS.AXPAS*1e-6*aF);//Velocidad m�xima alcanzada
    /*//DBG
        std::cout<<"A.N: "<<AXIS.A.N<<std::endl;
        std::cout<<"A.NI: "<<AXIS.A.NI<<std::endl;        
        std::cout<<"nS: "<<nS<<std::endl;
        std::cout<<"aF: "<<aF<<std::endl;
        std::cout<<"dS: "<<dS<<std::endl;
        std::cout<<"F.NR: "<<AXIS.F.NR<<std::endl;
    //DBG*/
    //Speed and aceleration initialization
    
    /*//DBG
    std::cout<<"xd: "<<xd<<std::endl;
    std::cout<<"yd: "<<yd<<std::endl;
    std::cout<<"zd: "<<zd<<std::endl;
    
    std::cout<<"XREL: "<<AXIS.X.AR<<std::endl;
    std::cout<<"YREL: "<<AXIS.Y.AR<<std::endl;
    std::cout<<"ZREL: "<<AXIS.Z.AR<<std::endl;
    
    std::cout<<"AXMDEL: "<<AXIS.MDEL<<std::endl;
    std::cout<<"AXIS.F.NI: "<<AXIS.F.NI<<std::endl;
    //DBG*/
    
    usleep(IDEL);//Calentando electronica, estabilizaci�n segura de se�al.
    
}

// Prototipo de Ejecutador


if(cx<=cy&&cx<=cz&&AXIS.X.A!=AXIS.X.AI){    
    GPIO_SET = 1<<XS;//1            
    usleep(IDEL);         
    GPIO_CLR = 1<<XS;//0 
    //std::cout<<"cc"<<AXPAS*xd<<" , "<<xd;
    AXIS.X.A+=AXIS.AXPAS*xd;
    cx+=AXIS.X.AR;
       nC+=1;//Velocidad y aceleracion
}

if(cy<=cx&&cy<=cz&&AXIS.Y.A!=AXIS.Y.AI){    
    GPIO_SET = 1<<YS;//1            
    usleep(IDEL);         
    GPIO_CLR = 1<<YS;//0 
    
    AXIS.Y.A+=AXIS.AXPAS*yd;
    cy+=AXIS.Y.AR;
     nC+=1;//Velocidsad y aceleracion
}

if(cz<=cx&&cz<=cy&&AXIS.Z.A!=AXIS.Z.AI){    
    GPIO_SET = 1<<ZS;//1            
    usleep(IDEL);         
    GPIO_CLR = 1<<ZS;//0 
    
    AXIS.Z.A+=AXIS.AXPAS*zd;
    cz+=AXIS.Z.AR;
     nC+=1;//Velocidad y aceleracion
}


//manejo de alceleracion y deceleracion
   /* if(AXIS.pI()<=0.5){
          if(AXIS.F.NI<=AXIS.F.N){
              AXIS.F.N+=AXIS.A.NI*TS.elapsedTime;
        AXIS.MDEL=(int)((((AXPAS*1e-6)/(AXIS.F.N)-(7.507507507e-6))/(18e-9)); 
    if(AXIS.MDEL<=0){AXIS.MDEL=0;}
    }  
    }
    else{
        if(AXIS.F.NI<=AXIS.F.N){
            
        }
    }*/

    if(nC<=aF){
        //AXIS.F.N+=(int)(AXIS.A.NR*sqrt(nC));
        AXIS.F.N=(sqrt(nC*(AXIS.AXPAS*1e-6)*2*AXIS.A.NI));
        AXIS.VC();
    }
    else if(nC>dS){
        //AXIS.F.N=(int)AXIS.A.NR*sqrt(nC-dS);
        AXIS.F.N=(1+AXIS.F.NR)-(sqrt(((1+nC)-dS)*(AXIS.AXPAS*1e-6)*2*AXIS.A.NI));
        AXIS.VC();
    }
//manejo de alceleracion y deceleracion


// Prototipo de Ejecutador*/

/*//RELATIVE EXECUTOR
AXIS.X.AR=AXIS.AXRELF(AXIS.X.AS,AXIS.X.A,AXIS.X.AI);
AXIS.Y.AR=AXIS.AXRELF(AXIS.Y.AS,AXIS.Y.A,AXIS.Y.AI);
AXIS.Z.AR=AXIS.AXRELF(AXIS.Z.AS,AXIS.Z.A,AXIS.Z.AI);
    

if(AXIS.X.A<AXIS.X.AI&&AXIS.X.AR<=AXIS.Y.AR&&AXIS.X.AR<=AXIS.Z.AR){
//inst[0]='X';
    //GPIO_SET = 1<<XE;//1
    //usleep(EDEL);
    
    GPIO_SET = 1<<XS;//1            
    usleep(IDEL);         
    GPIO_CLR = 1<<XS;//0     
    
   // GPIO_CLR = 1<<XE;//0
    
AXIS.X.A+=AXPAS;

}

else if(AXIS.X.A>AXIS.X.AI&&AXIS.X.AR<=AXIS.Y.AR&&AXIS.X.AR<=AXIS.Z.AR){
//inst[0]='x';
    //GPIO_SET = 1<<XE;//1        
   // GPIO_SET = 1<<XRS;//1
    //usleep(EDEL);
    
    GPIO_SET = 1<<XS;//1            
    usleep(IDEL);    
    GPIO_CLR = 1<<XS;//0  
    
    //GPIO_CLR = 1<<XRS;//0 
    //GPIO_CLR = 1<<XE;//0     
    
AXIS.X.A-=AXPAS;

}

else if(AXIS.Y.A<AXIS.Y.AI&&AXIS.Y.AR<=AXIS.X.AR&&AXIS.Y.AR<=AXIS.Z.AR){
//inst[0]='Y';
    //GPIO_SET = 1<<XE;//1
   // usleep(EDEL);    
    
    GPIO_SET = 1<<YS;//1            
    usleep(IDEL);         
    GPIO_CLR = 1<<YS;//0   
AXIS.Y.A+=AXPAS;

}

else if(AXIS.Y.A>AXIS.Y.AI&&AXIS.Y.AR<=AXIS.X.AR&&AXIS.Y.AR<=AXIS.Z.AR){
//inst[0]='y';
    GPIO_SET = 1<<YS;//1            
    usleep(IDEL);         
    GPIO_CLR = 1<<YS;//0   
AXIS.Y.A-=AXPAS;

}

else if(AXIS.Z.A<AXIS.Z.AI&&AXIS.Z.AR<=AXIS.X.AR&&AXIS.Z.AR<=AXIS.Y.AR){
//inst[0]='Z';
    GPIO_SET = 1<<ZS;//1            
    usleep(IDEL);         
    GPIO_CLR = 1<<ZS;//0   
AXIS.Z.A+=AXPAS;

}

else if(AXIS.Z.A>AXIS.Z.AI&&AXIS.Z.AR<=AXIS.X.AR&&AXIS.Z.AR<=AXIS.Y.AR){
//inst[0]='z';
    GPIO_SET = 1<<ZS;//1            
    usleep(IDEL);         
    GPIO_CLR = 1<<ZS;//0   
AXIS.Z.A-=AXPAS;
}


else if(AXIS.X.A<AXIS.X.AI){
//inst[0]='X';
    //GPIO_SET = 1<<XE;//1
    //usleep(EDEL);
    
    GPIO_SET = 1<<XS;//1            
    usleep(IDEL);         
    GPIO_CLR = 1<<XS;//0     
    
    //GPIO_CLR = 1<<XE;//0
    
AXIS.X.A+=AXPAS;

}

else if(AXIS.X.A>AXIS.X.AI){
//inst[0]='x';
    //GPIO_SET = 1<<XE;//1        
    //GPIO_SET = 1<<XRS;//1
    //usleep(EDEL);
    
    GPIO_SET = 1<<XS;//1            
    usleep(IDEL);    
    GPIO_CLR = 1<<XS;//0  
    
    //GPIO_CLR = 1<<XRS;//0 
   // GPIO_CLR = 1<<XE;//0     
    
AXIS.X.A-=AXPAS;

}

else if(AXIS.Y.A<AXIS.Y.AI){
//inst[0]='Y';
    GPIO_SET = 1<<YS;//1            
    usleep(IDEL);         
    GPIO_CLR = 1<<YS;//0   
AXIS.Y.A+=AXPAS;

}

else if(AXIS.Y.A>AXIS.Y.AI){
//inst[0]='y';
    GPIO_SET = 1<<YS;//1            
    usleep(IDEL);         
    GPIO_CLR = 1<<YS;//0   
AXIS.Y.A-=AXPAS;

}

else if(AXIS.Z.A<AXIS.Z.AI){
//inst[0]='Z';
    GPIO_SET = 1<<ZS;//1            
    usleep(IDEL);         
    GPIO_CLR = 1<<ZS;//0   
AXIS.Z.A+=AXPAS;

}

else if(AXIS.Z.A>AXIS.Z.AI){
//inst[0]='z';
    GPIO_SET = 1<<ZS;//1            
    usleep(IDEL);         
    GPIO_CLR = 1<<ZS;//0   
AXIS.Z.A-=AXPAS;

}//*/

AXIS.rD();//necesario para que los datos de otros programas no sean interceptados
AXIS.uDS();
AXIS.wD();

if(AXIS.I()){// End of the instruction execution
   // std::cout<<"No inst"<<std::endl;
    AXIS.IS.SNCB(0);//Incrementa el semaforo en 1 bloqueandolo cuando la instrucción se ha completado
    
    AXIS.IS.SNCF(1); //Libera el semaforo de procesando instrucción.
    
    //std::cout<<"INS_COMP"<<std::endl;
}

usleep(AXIS.MDEL+IDEL);//Implementacion del retraso maestro

        /* //Motor controler test
    
          GPIO_SET = 1<<XE;//1
              usleep(100);
      
    for(int f=0;f<1600;f++){        
        GPIO_SET = 1<<XS;//1            
        usleep(1604-f);     
        
        GPIO_CLR = 1<<XS;//0        
        usleep(1604-f);
    
    }
    
    for(int f=0;f<1600;f++){        
        GPIO_SET = 1<<XS;//1            
        usleep(5+f);     
        
        GPIO_CLR = 1<<XS;//0        
        usleep(5+f);
    
    }
    
    GPIO_SET = 1<<XRS;//1
    usleep(500000); 
            
    for(int r=0;r<1600;r++){ 
        GPIO_SET = 1<<XS;//1            
        usleep(1604-r);  
    
        GPIO_CLR = 1<<XS;//0
        usleep(1604-r);  

    }
    
    for(int r=0;r<1600;r++){ 
        GPIO_SET = 1<<XS;//1            
        usleep(5+r);  
    
        GPIO_CLR = 1<<XS;//0
        usleep(5+r);  

    }
    GPIO_CLR = 1<<XRS;//0
    usleep(500000);
    
  
    GPIO_CLR = 1<<XE;//0
    usleep(100);  

    
        //Motor controler test*/
    
        /* //Electronics debugger
         
             usleep(50000);
             
             GPIO_SET = 1<<XS;//1
             GPIO_SET = 1<<XRS;//1             
             GPIO_SET = 1<<XE;//1

             GPIO_SET = 1<<YS;//1
             GPIO_SET = 1<<YRS;//1             
             GPIO_SET = 1<<YE;//1
             
             GPIO_SET = 1<<ZS;//1
             GPIO_SET = 1<<ZRS;//1             
             GPIO_SET = 1<<ZE;//1             
             
             GPIO_SET = 1<<US;//1
             GPIO_SET = 1<<URS;//1             
             GPIO_SET = 1<<UE;//1
             
             GPIO_SET = 1<<VS;//1
             GPIO_SET = 1<<VRS;//1             
             GPIO_SET = 1<<VE;//1
             
             GPIO_SET = 1<<SS;//1
             GPIO_SET = 1<<SRS;//1   
             
             usleep(50000);
                
             GPIO_CLR = 1<<XS;//0
             GPIO_CLR = 1<<XRS;//0             
             GPIO_CLR = 1<<XE;//0

             GPIO_CLR = 1<<YS;//0
             GPIO_CLR = 1<<YRS;//0             
             GPIO_CLR = 1<<YE;//0
             
             GPIO_CLR = 1<<ZS;//0
             GPIO_CLR = 1<<ZRS;//0             
             GPIO_CLR = 1<<ZE;//0            
             
             GPIO_CLR = 1<<US;//0
             GPIO_CLR = 1<<URS;//0             
             GPIO_CLR = 1<<UE;//0
             
             GPIO_CLR = 1<<VS;//0
             GPIO_CLR = 1<<VRS;//0             
             GPIO_CLR = 1<<VE;//0
             
             GPIO_CLR = 1<<SS;//0
             GPIO_CLR = 1<<SRS;//0                
                
             
         
         //Electronics debugger*/

}
return 0;

} // main

void setup_io(){// Set up a memory regions to access GPIO
   /* open /dev/mem */
   if ((mem_fd = open("/dev/mem", O_RDWR|O_SYNC) ) < 0) {
      printf("can't open /dev/mem \n");
      exit(-1);
   }

   /* mmap GPIO */
   gpio_map = mmap(
      NULL,             //Any adddress in our space will do
      BLOCK_SIZE,       //Map length
      PROT_READ|PROT_WRITE,// Enable reading & writting to mapped memory
      MAP_SHARED,       //Shared with other processes
      mem_fd,           //File to map
      GPIO_BASE         //Offset to GPIO peripheral
   );

   close(mem_fd); //No need to keep mem_fd open after mmap

   if (gpio_map == MAP_FAILED) {
      printf("mmap error %d\n", (int)gpio_map);//errno also set!
      exit(-1);
   }

   // Always use volatile pointer!
   gpio = (volatile unsigned *)gpio_map;
} // setup_io

void signalHandler(int sig) {// Set up a signal handler to catch ctrl-c
  printf("Ctrl-C pressed, exiting!\n");
  
  GPIO_CLR = 1<<XS;
  GPIO_CLR = 1<<XRS;
  GPIO_CLR = 1<<XE;
  
  GPIO_CLR = 1<<YS;
  GPIO_CLR = 1<<YRS;
  GPIO_CLR = 1<<YE;
  
  GPIO_CLR = 1<<ZS;
  GPIO_CLR = 1<<ZRS;
  GPIO_CLR = 1<<ZE;
  
  GPIO_CLR = 1<<US;
  GPIO_CLR = 1<<URS;
  GPIO_CLR = 1<<UE;
  
  GPIO_CLR = 1<<VS;
  GPIO_CLR = 1<<VRS;
  //GPIO_CLR = 1<<VE;
  
  GPIO_CLR = 1<<SS;
  GPIO_CLR = 1<<SRS;
  GPIO_CLR = 1<<SE;
  
  exit(0);
}
