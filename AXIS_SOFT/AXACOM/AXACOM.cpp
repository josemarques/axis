#include <stdio.h>      // standard input / output functions
#include <stdlib.h>
#include <string.h>     // string function definitions
#include <unistd.h>     // UNIX standard function definitions
#include <fcntl.h>      // File control definitions
#include <errno.h>      // Error number definitions
#include <termios.h>
#include <iostream>
#include <math.h>


#include "../LIB/AX.h"

const int AXPAS=5;

const int SDEL=2000;//Delay testado, correcto
//const int SDEL=1700;//Delay testado, correcto
//const int SDEL=1000;//Delay experimental

int n_written=0;
int n=0;

bool run=true;


double AXRELF(float s,float r,float i){
    double S=s,R=r,I=i;
if(abs(S-I)==0) return 1;//X INSTRUCTION RELATION
else{
double TR=abs(S-R);
double TRI=abs(S-I);
double RE=TR/TRI;
/*if(RE>=1)return 1;
else*/ return RE;}
}

/* Open File Descriptor */
int main(){
std::cout << "AXACOM V0.42 C++" << std::endl;

char data [1];
char inst [1];

char bufc [2048];

char SD[729];

std::string SerialData;

//inst[1]='\n';
//inst[2]='\n';

int idse= ISE ();
int idsmi=ISMI ();
int idsmd=ISMD ();

int idsnc=SNCI();//Semaforo en 0 supuestamente


AXH AXIS(idse,idsmi,idsmd/*, idsnc*/);

int USB = open( "/dev/ttyACM0", O_RDWR| O_NOCTTY );

/* Error Handling */
if ( USB < 0 ){
std::cout << "Error " << errno << " opening " << "/dev/ttyACM0" << ": " << strerror (errno) << std::endl;
run=false;
}

/* *** Configure Port *** */
struct termios tty;
memset (&tty, 0, sizeof tty);

/* Error Handling */
if ( tcgetattr ( USB, &tty ) != 0 )
std::cout << "Error " << errno << " from tcgetattr: " << strerror(errno) << std::endl;

/* Set Baud Rate */
cfsetispeed (&tty, B921600);
cfsetispeed (&tty, B921600);
/* PORT CONFIG */
tty.c_cflag     &=  ~PARENB;        // Make 8n1
tty.c_cflag     &=  ~CSTOPB;
tty.c_cflag     &=  ~CSIZE;
tty.c_cflag     |=  CS8;
tty.c_cflag     &=  ~CRTSCTS;       // no flow control
tty.c_lflag     =   0;          // no signaling chars, no echo, no canonical processing
tty.c_oflag     =   ~OPOST;                  // no remapping, no delays
tty.c_cc[VMIN]      =  0;                  // read doesn't block
tty.c_cc[VTIME]     =  0;                  // 0.5 seconds read timeout

tty.c_cflag     |=  CREAD | CLOCAL;     // turn on READ & ignore ctrl lines
tty.c_iflag     &=  ~(IXON | IXOFF | IXANY);// turn off s/w flow ctrl
tty.c_lflag     &=  ~(ICANON | ECHO | ECHOE | ISIG); // make raw
tty.c_oflag     &=  ~OPOST;              // make raw

/* Flush Port, then applies attributes */
tcflush( USB, TCIFLUSH );

if ( tcsetattr ( USB, TCSANOW, &tty ) != 0){
std::cout << "Error " << errno << " from tcsetattr" << std::endl;
}

AXIS.rI();
AXIS.rD();

while(run){
   
 SNCR(idsnc,0);//Se bloquea hasta que el semaforo esté en 0
 
 
AXIS.rIa();


//AXIS.rSd();

//AXIS.X.A=GETGDR("X",AXIS.AXd,AXIS.X.A);
//AXIS.Y.A=GETGDR("Y",AXIS.AXd,AXIS.Y.A);
//AXIS.Z.A=GETGDR("Z",AXIS.AXd,AXIS.Z.A);





//AXIS.X.AI=RIP(AXIS.X.AI,5);
//AXIS.Y.AI=RIP(AXIS.Y.AI,5);
//AXIS.Z.AI=RIP(AXIS.Z.AI,5);



AXIS.X.AR=AXRELF(AXIS.X.AS,AXIS.X.A,AXIS.X.AI);
AXIS.Y.AR=AXRELF(AXIS.Y.AS,AXIS.Y.A,AXIS.Y.AI);
AXIS.Z.AR=AXRELF(AXIS.Z.AS,AXIS.Z.A,AXIS.Z.AI);


//RELATIVE EXECUTOR
if(AXIS.X.A<AXIS.X.AI&&AXIS.X.AR<=AXIS.Y.AR&&AXIS.X.AR<=AXIS.Z.AR){
inst[0]='X';

AXIS.X.A+=AXPAS;

}

else if(AXIS.X.A>AXIS.X.AI&&AXIS.X.AR<=AXIS.Y.AR&&AXIS.X.AR<=AXIS.Z.AR){
inst[0]='x';

AXIS.X.A-=AXPAS;

}

else if(AXIS.Y.A<AXIS.Y.AI&&AXIS.Y.AR<=AXIS.X.AR&&AXIS.Y.AR<=AXIS.Z.AR){
inst[0]='Y';

AXIS.Y.A+=AXPAS;

}

else if(AXIS.Y.A>AXIS.Y.AI&&AXIS.Y.AR<=AXIS.X.AR&&AXIS.Y.AR<=AXIS.Z.AR){
inst[0]='y';

AXIS.Y.A-=AXPAS;

}

else if(AXIS.Z.A<AXIS.Z.AI&&AXIS.Z.AR<=AXIS.X.AR&&AXIS.Z.AR<=AXIS.Y.AR){
inst[0]='Z';

AXIS.Z.A+=AXPAS;

}

else if(AXIS.Z.A>AXIS.Z.AI&&AXIS.Z.AR<=AXIS.X.AR&&AXIS.Z.AR<=AXIS.Y.AR){
inst[0]='z';

AXIS.Z.A-=AXPAS;
}


else if(AXIS.X.A<AXIS.X.AI){
inst[0]='X';

AXIS.X.A+=AXPAS;

}

else if(AXIS.X.A>AXIS.X.AI){
inst[0]='x';

AXIS.X.A-=AXPAS;

}

else if(AXIS.Y.A<AXIS.Y.AI){
inst[0]='Y';

AXIS.Y.A+=AXPAS;

}

else if(AXIS.Y.A>AXIS.Y.AI){
inst[0]='y';

AXIS.Y.A-=AXPAS;

}

else if(AXIS.Z.A<AXIS.Z.AI){
inst[0]='Z';

AXIS.Z.A+=AXPAS;

}

else if(AXIS.Z.A>AXIS.Z.AI){
inst[0]='z';

AXIS.Z.A-=AXPAS;

}


n_written = write( USB, inst, sizeof (inst));
inst[0]='n';

usleep(AXIS.F.AI+SDEL);
AXIS.F.A=AXIS.F.AI;



//n = read( USB, &bufc , sizeof (bufc) );

/*for(int w=0;w<sizeof (bufc);w++){
 if(bufc[w]=='X')AXIS.X.A+=AXPAS;
else if(bufc[w]=='x')AXIS.X.A-=AXPAS;
else if(bufc[w]=='Y')AXIS.Y.A+=AXPAS;
else if(bufc[w]=='y')AXIS.Y.A-=AXPAS;
else if(bufc[w]=='Z')AXIS.Z.A+=AXPAS;
else if(bufc[w]=='z')AXIS.Z.A-=AXPAS;

}*/

//for(int i=0;i<sizeof bufc;i++)bufc[i]=0;


//AXIS.rSd();
AXIS.rSd();//necesario para que los datos de otros programas no sean interceptados


//AXIS.uD();
AXIS.AXd=ADDGDS(AXIS.AXd,"X",AXIS.X.A);
AXIS.AXd=ADDGDS(AXIS.AXd,"Y",AXIS.Y.A);
AXIS.AXd=ADDGDS(AXIS.AXd,"Z",AXIS.Z.A);

AXIS.wD();



if(AXIS.I()){
   // std::cout<<"No inst"<<std::endl;
    SNCB(idsnc,0);//Incrementa el semaforo en 1 bloqueandolo cuando la instrucción se ha completado
    
    SNCF(idsnc,1); //Libera el semaforo de procesando instrucción.

}



}
//END RELATIVE EXECUTOR




close(USB);
}
